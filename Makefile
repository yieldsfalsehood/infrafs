
SNAME = infrafs

INITOS = initos/initos.initrd
LINUX = linux/linux-$(LINUX_VERSION)-$(LINUX_ARCH).bzImage

.PHONY: all
all:
	@$(MAKE) jsh

.PHONY: jsh
jsh:
	@$(MAKE) -C jsh

develop:
	@pip install -Ur requirements.txt

flake8:
	@python -m flake8 src

pylint:
	@PYTHONPATH=src python -m pylint src

install:
	@pip install .

.PHONY: initos
initos:
	@$(MAKE) -C initos

.PHONY: clean-initos
clean-initos:
	@$(MAKE) -C initos clean

.PHONY: build-kernel
build-kernel:
	git format-archive | sh initos/kernel/build.sh > dist/kernel

.PHONY: clean
clean:
	@$(MAKE) clean-initos

%.ssh_host_rsa_key %.ssh_host_rsa_key.pub &:
	ssh-keygen -t rsa -C $* -N "" -f $*.ssh_host_rsa_key

%.key %-boot.qcow2 %-root.qcow2 &:
	bash scripts/install-host.sh $(LINUX) $(INITOS) $(PACKAGES) $(ALPINE) $*

.PHONY: install-host-%
install-host-%: $(LINUX) $(INITOS) $(ALPINE)
	$(MAKE) $*.ssh_host_rsa_key $*.ssh_host_rsa_key.pub
	$(MAKE) $*.key $*-boot.qcow2 $*-root.qcow2

.PHONY: configure-host-%
configure-host-%: $(LINUX) $(INITOS) $(ALPINE)
	bash scripts/configure-host.sh $(LINUX) $(INITOS) $(PACKAGES) $(ALPINE) $*

.PHONY: prepare-host-%
prepare-host-%: $(LINUX) $(INITOS) $(ALPINE)
	$(MAKE) install-host-$*
	$(MAKE) configure-host-$*

.PHONY: clean-host-%
clean-host-%:
	rm -f $*.key
	rm -f $*-{boot,root}.{img,qcow2}

%.pid: %.key %-boot.qcow2 %-root.qcow2
	bash start-host.sh $* &

.PHONY: start-host-%
start-host-%: %.pid

.PHONY: stop-host-%
stop-host-%:
	@[ -f $*.pid ] && pkill -F $*.pid || true

.PHONY: console-%
console-%:
	bash scripts/console.sh $*
