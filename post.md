
# infrafs: a demo of some tooling i've been working on

Hello! I've been working on some personal tooling that turned into a
whole bunch of stuff over the last few weeks that I thought I'd
share. This post is focused on prose and background and spans about
four projects at a high level. I'll link them in as they come up in
the narrative, but if you want to jump right to the technicals or see
the works in isolation they are:

1. [execvm](https://gitlab.com/yields.falsehood/execvm) a cli
   providing code generation and an abstraction over "executions" that
   is my current high level api I'm experimenting with
2. [scall](https://gitlab.com/yields.falsehood/scall) a small suite of
   system call wrappers I use as a sort of declarative shell
   replacement
3. [initos](https://gitlab.com/yields.falsehood/initos) a miniature
   "os" containing the above tools to support execvm's higher level
   virtualization
4. [infrafs](https://gitlab.com/yields.falsehood/infrafs) a little
   demo i built to see all these things in action and act as a sandbox
   for developing `execvm`

A copy of [this
post](https://gitlab.com/yields.falsehood/infrafs/-/blob/demo/post.md)
is available in the `infrafs` repo.

I've tried to make a point of adding some details including build
information in the `execvm` and `scall` readmes if you're curious for
more. The `initos` build probably won't work locally - I sacrificed
that in favor of getting the ci working. That said I did get some
packages built in the ci so technically you should be able to `pip
install execvm` from my gitlab repo. I don't have that well documented
yet in its own project, but the steps are in the `infrafs` README.

The `scripts/check.d` directory in both contain some example usages of
most of the tools if you'd like to see them in action in isolation. I
am realizing literally the morning of that it seems all the scripts in
the `example` directories are broken in both projects, though :) I
went through a big api change a few days ago and I guess I forgot
these haha. Still, the examples in `execvm` are valuable to see the
type constructors and yaml serialization I mention below in
action. Finally, I think the most interesting bits are in
`execvm.initos.initos`, where I generate commands to be run in a
virtualized guest, and `execvm.initos.subprocess`, where I orchestrate
the emulator, feeding it those commands.

Now, story time.

I have a little server in the basement I use for home labbing. It's
evolved as different areas come in and out of focus. I tend to cycle
interest fiddling with storage, networking, dns, virtualization, with
all kinds of automation pipe dreams. All the time I'm collecting
thoughts of how I wish it were all laid out, if only I had the time!

Everything's pretty stripped down right now so I'm not running much of
anything, but I have been experimenting with building out kubernetes
clusters. Step one of that was the same as every other home lab: start
some vms. I've done packer, expect, kickstart, cloud-init, libguestfs
every few years that I get the urge to come hack on things. This time
I got really hung up on lots of accumulated "I wonder if I could do
this differently".

## execvm

One of my first experiments was automated pfsense builds. I dug
through the bsd install scripts and figured out how to populate a
small `/etc/installerconfig` on the pfsense iso such that if I ran
qemu with this iso and a fresh empty image attached I'd wind up with a
blank pfsense installation!

That already feels too sticky, though :| I have to create a new iso
based on the original iso (introducing state, meh) and it strands me
to figure out my next step. How do I configure this guest after the
install runs? I could loop mount it and fuss with files on the local
filesystem, but then I'm escalating privileges and dealing with mounts
(more state, oy). I'd rather not have to have yet another service to
run that does this for me either because then I'm in a loop - how do I
automate provisioning and testing of guests running *that* thing? I
thought it'd be cool if after running the installer I could do
something like this:

```shell
$ tar -czf - | execvm run tar -xzf -
```

The idea was that I could `execvm run` some command in an emulated
kernel in a way that retains the type safety of vector execution while
minimizing the number of ad hoc "installer" isos/artifacts I might
need, i.e. I really would rather be able to inject the code at runtime
than have to "compile" the guest install logic in any way. So, I did a
rough POC to see that such a thing could actually work like I wanted,
and when I saw it could I got busy on these tools. However, for linux
only. I'm only just now realizing how far things have come in these
four weeks. All my attention was pfsense the first few days, now I'm
eye deep in linux :)

I implemented a complete demo at
[infrafs](https://gitlab.com/yields.falsehood/infrafs) that uses these
tools to install alpine into an encrypted disk image. I think that's
going to be my playground to focus on experimenting with the original
k8s ideas. I feel like the south park boys at the end of the war craft
episode finally geared up to tackle the original problem (although I
look a bit more like the other guy from the same episode after so much
time working on these).

In the demo you can see I encapsulate the alpine install logic (and
the subsequent host configuration logic) in lists of lists of strings:

```python
[
    ...
    ["/sbin/modprobe", "-a", "ext4"],
    ["/bin/mount", "-t", "iso9660", "-o", "ro", "/dev/sr0", "/mnt"],
    ["/sbin/apk", "add",
     "--repository", "file:///mnt/apks",
     "--keys-dir", "/etc/apk/keys",
     "--root", "/opt",
     "--initdb",
     "--no-cache",
     "--no-network",
     "--quiet",
     "--no-progress",
     "alpine-base",
     "alpine-baselayout",
     ],
    ...
]
```

The novelty is that from here until their execution they remain atomic
strings, even in the guest in which we're running the install. I never
embed them in a shell script nor interpolate these in anyway. I *do*
do (heh) lots of vector concatenation and generation, though! And to
that end, I added an interface that acts like `Itr[Str]` with lots of
types implementing it so that instead of always having to write things
like `["/bin/echo", "hello"]` I could have a type constructor like
`Echo("hello")` that expands to the same when used in iterator
contexts.

So, at its lowest level `execvm` is a code generation library that
builds lists of strings to execute and at its highest level is
`exec`'ing such a constructed list _somewhere_. It does that second
part by providing an encapsulation over "executions", really just
small types capturing the arguments to the `execve` family of
functions with a `.run()` interface to call the appropriate
`os.execv`. I also do yaml serialization! With tagging! All of that to
say that I hope you can start to imagine how "programs" could be
dynamically generated in python, encapsulated in yaml, and transmitted
to the guest, who could then unravel that back into an execution
object and call its `.run()` in the virtualized environment.

## scall

To do all that we need one more layer of abstraction. It's not enough
just to `exec` any ol' vector, I still need process sequencing so that
I can run each of those commands in order. Normally we'd let a shell
parse out newlines and spaces, but I opted to embed these commands in
the command lines of other commands provided by
[scall](https://gitlab.com/yields.falsehood/scall). For instance, this
`sequence` command would run each of these in order:

```shell
$ scall sequence
    -
    /bin/echo hello
    -
    /bin/echo world
    -
```

When `sequence` runs it parses those two commands out of its arguments
to run these two `exec` syscalls in this order:

```python
os.execv([
   "/bin/echo", "hello",
])
```

```python
os.execv([
   "/bin/echo", "world",
])
```

So, that snippet above from the alpine installation would actually
expand to a long sequence call like that, which when executed would
really run each step in order.

Other `scall` commands implement syscalls for opening, closing, and
duplicating file descriptors and for forking, which altogether lets me
implement file redirection and pipelining using process execution
instead of shell orchestration. (I actually felt like parts of the
`execvm run` code wound up reminding me of snippets from APUE as I
built up the different "replacement" parts I needed from those
primitives, which was kind of fun. I felt like I was in college again
haha)

The commands also all use the same ad hoc parsing library for (amongst
other things) extracting the "next command to run" from its arguments,
e.g. this closes stderr then `exec`s an `ls /proc/self/fd`:

```shell
$ scall close fd 2 - ls /proc/self/fd -
```

That brings us back to `execvm`, which provides matching
generation logic so I can write python code like this that reliably
embeds those commands:

```python
scall.Sequence([
    ["/bin/echo", "hello"],
    ["/bin/echo", "world"],
    ["/bin/echo", "foo"],
    ["/bin/echo", "bar"],
])
```

"Reliably" is important because I do nesty things like this:

```python
scall.Sequence([
    ["/bin/echo", "hello"],
    scall.Close(scall.Fd("2"),
                ["ls", "/proc/self/fd"]),
    ...,
])
```

Now it might make a little sense to see the yaml representation, too:

```yaml
!sequence
argvs:
  - ["/bin/echo", "hello"]
  - !close
    fd: !fd
      handle: "2"
    argv: ["ls", "/proc/self/fd"]
```

## initos

OK, *one* more layer of abstraction. I said earlier that the interface
I wanted during that pfsense install experiment was like `$ execvm run
tar -xf -`. I expected under the hood I'd have attached at least two
disks, one containing the orchestration tooling I'd need to support
doing this and the other the "actual"/"guest"/"application" disk - the
one in which we're running the tar. `initos` wound up taking the place
of that first thing.

This "OS" isn't really much of an OS. There is no package manager, and
it doesn't offer a traditional `init`. Instead, it is meant to act as
a programmable init so that I can tell it at runtime what to run next,
like this (with lots of wavy hands):

When the kernel starts he does some general initialization then tries
to start some next process. Typically that's an `init` in an
initramfs, which does some more initialization then starts *his* next
process. Typically that's an `init` in the intended root filesystem,
which typically has no next process! That's about where we'd say we've
booted and switch to some higher level interactive abstraction where
we wait for connections.

For `initos` specifically I use alpine's netboot kernel and initramfs
to boot to a squashfs I've published as a release on the
[initos](https://gitlab.com/yields.falsehood/initos) project
page. That contains a(nother) thin alpine installation that also
includes "execvm" and "scall". That's the magic - this "os" is just a
combination of those tools.

The nature of the `scall` tools is to take more commands to run on
their command lines and sequence them, so our `init` *does* have a
next thing to run. I do however need to do some amount of
synchronization, which is managed by `execvm`. He acts as a bridge to
pass an execution generated on the calling side into the guest, as
well as provides qemu orchestration and io wrangling so that
constructions like `a | execvm run b | c` do what you think.

## finally

OK! We're here! After weeks of iterating `scall` and `execvm` I got to
a reasonably working `initos`. I have a backlog a mile long for each
of those projects, but I'm not really sure yet where I go from here. I
might spend some time letting these lower level apis stew for a bit
while I hack on the ideas I started in `infrafs`, then come back to
iterate these again once I have some better use cases. However,
there's stuff I know that I want to do already, so I might be hacking
on these in parallel at least.

I think it'd be cool to see all the parsing stuff pulled out of
`scall` and into a haskell library. I also think a lot about seeing
`scall` replaced with a python implementation (maybe python making ffi
calls to a haskell library implementing the language
bits...). Finally, there's a lot of UX improvements to be had. Doing a
regular two phase boot to `initos` is expensive - I'm using lots of
RAM to load the initrd just to throw it away! IO syncrhonization is a
little wonky too because I'm forced to share the stdin
channel. There's a lot on my mind, but I'm excited. So we'll see!

Thoughts from the future, because my mind's still going :) Now I'm
wondering what it'd look like to ditch scall entirely from the
perspective of execvm. If the vector type materialized to llvm code
for instance we could just compile a complete program on the fly for
the init and bootstrap bits instead of having to pre-build the scall
stuff. I could eliminate all the parsing logic and the delimiter
finding bits, and I could probably extend a lil state into the
generator logic so when I pipe for instance I can actually carry those
values forward instead of the current "lol good luck guessing the
fds".

Thoughts from the future future! Another day away has helped shaped my
thoughts on this even more. I thought now was a good time to reflect
on how I got to where I am. I didn't set out to have three repos, in
fact I was trying to keep as little state as possible. That's how the
"define the init script at runtime" design emerged. At that time I
didn't yet know what my init needed to look like, so it was helpful to
be able to assemble one dynamically with scall. It occurred to me that
I don't need the scall binaries precompiled if I could interpret them
in a vm - yesterday I thought to native compile with llvm, then I
realized I already *have* a vm. Python! Rather than calling .vectorize
and exec'ing that, I believe I can massage my current api to add
something like a .execute api that would call os.pipe, os.dup2,
etc. Then I believe the overall design will simplify to just the one
execvm project, which can also then take over packaging initos
containing only execvm (which is nice for ci - they can share the same
tag version).

Hidden in all that text is this thought - "I didn't yet know what my
init needed to look like". Now that I know what a minimal working
example looks like, I think I'll use that to drive this new
design. That will give me an opportunity to think wholistically about
the boot protocol as well, which currently is awkwardly divided across
multiple indepdendent tools inside the guest.
