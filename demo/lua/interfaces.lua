
local function interface_options (hostname, interface)

   local options = {}

   for key in python.iter(python.as_attrgetter(interface).get_dict("options")) do
       options[key] = interface.options[key]
   end

   if interface.method == "dhcp" and options.hostname == nil then
      options.hostname = hostname
   end

   return options

end

local function interfaces_fragments (g)

   local fragments = {}

   for name in python.iter(g.c.host.interfaces) do
      interface = g.c.host.interfaces[name]
      fragments[name] = g.template "templates/interfaces.j2" {
         name = name,
         auto = python.as_attrgetter(interface).get("auto", true),
         family = interface.family,
         method = interface.method,
         options = interface_options(g.c.hostname, interface),
      }
   end

   return fragments

end

return function (g)

   g.fs.file "/etc/network/interfaces" {
      content = g.buffer(interfaces_fragments(g))
   }

end
