
paths = require("paths")

local function import (g, keyring, fingerprint)

   g.exec {"gpg", "--homedir", keyring,
           "--import", g.path(keyring, fingerprint..".key")}
   g.exec {"gpg", "--homedir", keyring,
           "--quick-lsign-key", fingerprint}

end

local function verify (g, keyring, fingerprint)
   g.exec {"gpg", "--homedir", keyring,
           "--verify",
           g.path(keyring, fingerprint..".key.asc"),
           g.path(keyring, fingerprint..".key")}
end

return function (g)

   local keyring = paths.keyring(g, "gnupg")

   g.exec {"gpg", "--homedir", keyring,
           "--batch",
           "--passphrase", "",
           "--quick-gen-key",
           g.c.signer}

   import(g, keyring, g.c.trusted)
   verify(g, keyring, g.c.trusted)

   for fingerprint in python.iter(g.c.keys) do
      verify(g, keyring, fingerprint)
      import(g, keyring, fingerprint)
   end

end
