
fhs = require("cpace/initos/fhs")
busybox = require("cpace/initos/busybox")
jsh = require("cpace/initos/jsh")
init = require("cpace/initos/init")

interfaces = require("cpace/interfaces")

return function (g)

   fhs(g)
   busybox(g)

   jsh(g)

   init(g)

   interfaces(g)

   g.fs.file "/etc/mactab" {
      content = g.template "templates/mactab.j2" {
         interfaces = g.c.host.interfaces
      }
   }

   g.fs.file "/etc/passwd" {
      content = g.buffer({"root:x:0:0:root:/root:/bin/bash"}),
   }

   g.fs.file "/etc/group" {
      content = g.buffer({"root:x:0:root"}),
   }

   g.fs.file "/etc/shadow" {
      content = g.buffer({"root:::0:::::"}),
   }

end
