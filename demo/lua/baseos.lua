
return function (g)

   -- base fs

   g.f.directory "/boot" {}

   -- system config

   g.f.file "/etc/inittab" {
      content = g.i:file("files/inittab"),
   }

   g.f.file "/etc/issue" {
      content = g.i:file("files/issue"),
   }

   g.f.file "/etc/motd" {
      content = g.i:file("files/motd"),
   }

   g.f.file "/etc/apk/repositories" {
      content = g.i:file("files/repositories"),
   }

end
