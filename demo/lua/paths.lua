
local paths = {}

function paths.keyring (G, name)
   return G.lib.path(G.datadir, "keyring", name)
end

return paths
