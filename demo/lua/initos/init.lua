return function (g)

   g.fs.file "/sbin/jetty" {
      mode = "0755",
      content = g.read("initos/jetty"),
   }

   g.fs.file "/usr/bin/autologin" {
      mode = "0755",
      content = g.read("initos/autologin"),
   }

   g.fs.file "/usr/bin/jogin" {
      mode = "0755",
      content = g.read("initos/jogin"),
   }

   g.fs.file "/init" {
      mode = "0755",
      content = g.read("initos/init"),
   }

   jsh(g)

end
