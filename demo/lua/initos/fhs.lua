
local function mkdirs (g, names)
   for name in pairs(names) do
      g.fs.directory(name)({uid="0", gid="0", mode="0755"})
   end
end

return function (g)

   -- I bootstrapped this from arch using the directories from their
   -- filesystem package:
   --   `pkgfile -l filesystem | grep '/$'`

   mkdirs(g, {
      "/boot",
      "/dev",
      "/etc",
      "/etc/ld.so.conf.d",
      "/etc/profile.d",
      "/etc/skel",
      "/home",
      "/mnt",
      "/opt",
      "/proc",
      "/root",
      "/run",
      "/srv",
      "/srv/ftp",
      "/srv/http",
      "/sys",
      "/tmp",
      "/usr",
      "/usr/bin",
      "/usr/include",
      "/usr/lib",
      "/usr/lib/sysctl.d",
      "/usr/lib/systemd",
      "/usr/lib/systemd/system-environment-generators",
      "/usr/lib/sysusers.d",
      "/usr/lib/tmpfiles.d",
      "/usr/local",
      "/usr/local/bin",
      "/usr/local/etc",
      "/usr/local/games",
      "/usr/local/include",
      "/usr/local/lib",
      "/usr/local/man",
      "/usr/local/sbin",
      "/usr/local/share",
      "/usr/local/src",
      "/usr/share",
      "/usr/share/factory",
      "/usr/share/factory/etc",
      "/usr/share/man",
      "/usr/share/man/man1",
      "/usr/share/man/man2",
      "/usr/share/man/man3",
      "/usr/share/man/man4",
      "/usr/share/man/man5",
      "/usr/share/man/man6",
      "/usr/share/man/man7",
      "/usr/share/man/man8",
      "/usr/share/misc",
      "/usr/share/pixmaps",
      "/usr/src",
      "/var",
      "/var/cache",
      "/var/empty",
      "/var/games",
      "/var/lib",
      "/var/lib/misc",
      "/var/local",
      "/var/log",
      "/var/log/old",
      "/var/opt",
      "/var/spool",
      "/var/spool/mail",
      "/var/tmp",
   })

-- g.fs.link "/bin" {uid="0", gid="0", mode="0755", link="/usr/bin"}
-- g.fs.link "/sbin" {uid="0", gid="0", mode="0755" link="/usr/bin"}
-- g.fs.link "/usr/sbin" ({uid="0", gid="0", mode="0755" link="/usr/bin"}

end
