
return function (g)

   -- should pull busybox ref from config
   g.fs.file "/bin/busybox" {
      user = "root",
      group = "root",
      mode = "0755",
      content = g.read("busybox/1.31.0-defconfig-multiarch-musl/busybox-x86_64"),
   }

end
