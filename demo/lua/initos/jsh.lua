return function (g)

   -- should pull jsh ref from config
   g.fs.file "/usr/bin/jsh" {
      mode = "0755",
      content = g.read("jsh/jsh"),
   }

end
