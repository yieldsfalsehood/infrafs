
local execvm = require("execvm")

local function import (keyringdir, fingerprint)

   coroutine.yield(
      execvm.command {
         argv={
            "gpg", "--homedir", keyringdir,
            "--import", execvm.path(keyringdir, fingerprint..".key")
         }
      }
   )

   coroutine.yield(
      execvm.command {
         argv={
            "gpg", "--homedir", keyringdir,
            "--quick-lsign-key", fingerprint
         }
      }
   )

end

local function verify (keyringdir, fingerprint)

   coroutine.yield(
      execvm.command {
         argv={
            "gpg", "--homedir", keyringdir,
            "--verify",
            execvm.path(keyringdir, fingerprint..".key.asc"),
            execvm.path(keyringdir, fingerprint..".key")
         }
      }
   )

end

local function files (keyring, keyringdir, filesdir)

   local result = {}

   for _, fingerprint in pairs(keyring.keys) do

      table.insert(
         result,
         execvm.tar.file {
            name = execvm.path(keyringdir, fingerprint..".key"),
            contents = execvm.file {
               name = execvm.path(filesdir, fingerprint..".key")
            }
         }
      )

      table.insert(
         result,
         execvm.tar.file {
            name = execvm.path(keyringdir, fingerprint..".key.asc"),
            contents = execvm.file {
               name = execvm.path(filesdir, fingerprint..".key.asc")
            }
         }
      )

   end

   return result

end

return function (args, values)

   local datadir = args[1]
   local name = args[2]

   local keyring = values.keyring[name]
   local keyringdir = execvm.path(datadir, "keyring", name)
   local filesdir = execvm.path("demo/files/keyring", name)

   coroutine.yield(
      execvm.tar {
         entries = files(keyring, keyringdir, filesdir),
         target = execvm.command {
            argv={
               "tar", "-xf", "-"
            }
         }
      }
   )

   coroutine.yield(
      execvm.command {
         argv={
            "gpg", "--homedir", keyringdir,
            "--batch",
            "--passphrase", "",
            "--quick-gen-key",
            keyring.signer
         }
      }
   )

   import(keyringdir, keyring.trusted)
   verify(keyringdir, keyring.trusted)

   for _, fingerprint in pairs(keyring.keys) do
      verify(keyringdir, fingerprint)
      import(keyringdir, fingerprint)
   end

end
