
local function major_version (version)
   -- x.y.z -> x.y
   return string.gsub(version, "(%d+[.]%d+)[.]%d+", "%1")
end

local function interface_options (hostname, interface)

   local options = {}

   for key in python.iter(python.as_attrgetter(interface).get_dict("options")) do
       options[key] = interface.options[key]
   end

   if interface.method == "dhcp" and options.hostname == nil then
      options.hostname = hostname
   end

   return options

end

local function interfaces_fragments (g)

   local fragments = {}

   for name in python.iter(g.c.host.interfaces) do
      interface = g.c.host.interfaces[name]
      fragments[name] = g.template "templates/interfaces.j2" {
         name = name,
         auto = python.as_attrgetter(interface).get("auto", true),
         family = interface.family,
         method = interface.method,
         options = interface_options(g.c.hostname, interface),
      }
   end

   return fragments

end

local function configure_network (g)

   g.fs.file "/etc/udev/rules.d/99-iface-names.rules" {
      content = g.template "templates/99-iface-names.rules.j2" {
         interfaces = g.c.host.interfaces
      }
   }

   g.fs.file "/etc/network/interfaces" {
      content = g.buffer(interfaces_fragments(g))
   }

end

return function (g)

   -- base fs

   g.fs.directory "/boot" {}

   -- system config

   g.fs.file "/etc/inittab" {
      content = g.read("files/inittab"),
   }

   g.fs.file "/etc/issue" {
      content = g.read("files/issue"),
   }

   g.fs.file "/etc/motd" {
      content = g.read("files/motd"),
   }

   -- g.fs.file "/etc/apk/repositories" {
   --    content = g.template "templates/repositories.j2" {
   --      version = major_version(os.getenv("ALPINE_VERSION"))
   --    }
   -- }

   g.fs.file "/etc/update-extlinux.conf" {
      content = g.template "templates/update-extlinux.conf.j2" {
         uuid = g.c.host.volumes.root.uuid
      }
   }

   g.fs.file "/etc/fstab" {
      content = g.template "templates/fstab.j2" {
         uuid = g.c.host.volumes.root.uuid
      },
   }

   g.fs.file "/etc/hostname" {
      content = g.template "templates/hostname.j2" {
         hostname = g.c.hostname
      }
   }

   configure_network(g)

   g.fs.file "/etc/conf.d/sshd" {
      content = g.read("files/sshd"),
   }

   g.fs.file "/etc/ssh/ssh_host_rsa_key" {
      mode = "0600",
      content = g.read("cluster1.workstation.ssh_host_rsa_key"),
   }

   g.fs.file "/etc/ssh/ssh_host_rsa_key.pub" {
      content = g.read("cluster1.workstation.ssh_host_rsa_key.pub"),
   }

   g.fs.file "/etc/ssh/ssh_known_hosts" {
      content = g.template "templates/known_hosts.j2" {}
   }

   g.fs.file "/root/.ssh/id_rsa" {
      mode = "0600",
      content = g.read("files/id_rsa"),
   }

   g.fs.file "/etc/sudoers.d/00-user" {
      content = g.read("files/sudoers"),
   }

   -- user-specific config

   g.fs.file "/etc/passwd" {
      content = g.template "files/passwd" {},
   }

   g.fs.file "/etc/group" {
      content = g.template "files/group" {},
   }

   g.fs.file "/etc/shadow" {
      content = g.template "files/shadow" {},
   }

   g.fs.directory "/home/admin" {
      user = "admin",
      group = "admin",
      mode = "0700",
   }

   g.fs.directory "/home/admin/.ssh" {
      user = "admin",
      group = "admin",
      mode = "0700",
   }

   g.fs.file "/home/admin/authorized_keys" {
      user = "admin",
      group = "admin",
      mode = "0600",
      content = g.read("files/id_rsa.pub"),
   }

   g.fs.file "/usr/bin/jsh" {
      mode = "0755",
      content = g.read("jsh/jsh"),
   }

end
