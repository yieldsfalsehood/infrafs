#!/bin/bash

make -C jsh vet
make -C jsh format-source

make develop
make flake8
make pylint
