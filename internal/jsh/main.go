package jsh

import (
	"encoding/json"
	"gitlab.com/yieldsfalsehood/infrafs/initos/jsh/internal/protocol"
	"io"
	"log"
	"os"
	// "os/exec"
	"sync"
)

func dispatch(env protocol.Envelope, mqout chan protocol.SendableMessage, ptable protocol.ProcessTable, pg *sync.WaitGroup) (bool, error) {

	m, err := protocol.NewReceivableMessage(env)

	if err != nil {
		// echo
		return false, err
	}

	ok := m.Receive(mqout, ptable, pg)

	return ok, nil

}

func recvMessages(done chan bool, mqout chan protocol.SendableMessage, ptable protocol.ProcessTable, pg *sync.WaitGroup) {

	decoder := json.NewDecoder(os.Stdin)

	for {

		var env protocol.Envelope

		if err := decoder.Decode(&env); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		if ok, err := dispatch(env, mqout, ptable, pg); err != nil {
			break
		} else if !ok {
			break
		}

	}

	done <- true

}

func sendMessages(done chan bool, mqout chan protocol.SendableMessage) {

	encoder := json.NewEncoder(os.Stdout)

	for {
		if m, more := <-mqout; !more {
			break
		} else {
			env, err := protocol.NewEnvelope(m)
			if err != nil {
				log.Fatal(err)
			}
			// log.Print("encoding", m.Kind(), m.Trace())
			encoder.Encode(env)
			// log.Print("encoded!")
		}
	}

	done <- true

}

func init() {
}

func Main(args []string) {

	doneReceiving := make(chan bool, 1)
	doneSending := make(chan bool, 1)

	mqout := make(chan protocol.SendableMessage)

	ptable := make(protocol.ProcessTable)
	var pg sync.WaitGroup

	go recvMessages(doneReceiving, mqout, ptable, &pg)
	go sendMessages(doneSending, mqout)

	mqout <- protocol.HeloMessage{}

	<-doneReceiving

	pg.Wait()
	close(mqout)

	<-doneSending

}
