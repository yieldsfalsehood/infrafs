package protocol

import (
	// "bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	// "os"
	"math/rand"
	"os/exec"
	// "errors"
	// "fmt"
	// "reflect"
	// "strconv"
	"sync"
)

const (
	protocolVersion = "1.0"
	STDOUT          = 1
	STDERR          = 2
)

type Process struct {
	Command *exec.Cmd
	Stdin   io.WriteCloser
}

type ProcessTable map[int]Process

type OutputForwarder struct {
	Mqout  chan SendableMessage
	Jid    int
	Stream int
}

func (of OutputForwarder) Write(p []byte) (n int, err error) {

	// go's stdout/err monitor apparently will use the same
	// internal buffer for each of its reads, and will pass a
	// reference to that buffer to us. if the contents of that
	// buffer are large (for instance, i've seen a 32kB output
	// buffer when running `cat`) then the transport encoder loop
	// might be blocked writing while the output monitor loop
	// overwrites the buffer contents. i don't think i can
	// reliably synchronize around that, so i'm just copying the
	// data each time to be certain. OutputMessage is the only
	// message type with this concern right now, as all others use
	// unshared strings and ints for their members.
	//
	// https://stackoverflow.com/a/27208802

	sid := rand.Uint32()
	// log.Print("output", sid, string(p))
	of.Mqout <- OutputMessage{
		Jid:     of.Jid,
		Stream:  of.Stream,
		Sid:     sid,
		Payload: append([]byte(nil), p...),
	}
	// log.Print("sent!")
	return len(p), nil

}

func monitorProcess(mqout chan SendableMessage, pg *sync.WaitGroup, command *exec.Cmd, jid int) {
	command.Wait()
	mqout <- StoppedMessage{Jid: jid, Pid: command.Process.Pid, Status: command.ProcessState.ExitCode()}
	pg.Done()
}

// Message represents a jsh protocol message
//
// Version: must always be set to "1.0" for protocol version 1.0
//
// Kind: string designating the message type
//
// Params: json object
type Envelope struct {
	Version string          `json:"version"`
	Kind    string          `json:"kind"`
	Params  json.RawMessage `json:"params,omitempty"`
}

type SendableMessage interface {
	Kind() string
	Trace() string
}

type ReceivableMessage interface {
	Receive(mqout chan SendableMessage, ptable ProcessTable, pg *sync.WaitGroup) bool
}

func NewReceivableMessage(env Envelope) (ReceivableMessage, error) {

	var m ReceivableMessage

	switch env.Kind {
	case "start":
		m = new(StartMessage)
	case "input":
		m = new(InputMessage)
	case "eof":
		m = new(EOFMessage)
	case "bye":
		m = new(ByeMessage)
	}

	err := json.Unmarshal(env.Params, m)

	if err != nil {
		return nil, err
	}

	return m, nil

}

func NewEnvelope(message SendableMessage) (*Envelope, error) {

	marshaled, err := json.Marshal(message)

	if err != nil {
		return nil, err
	}

	embedded := json.RawMessage(marshaled)

	envelope := &Envelope{
		Kind:    message.Kind(),
		Params:  embedded,
		Version: protocolVersion,
	}

	return envelope, nil

}

type HeloMessage struct {
}

func (HeloMessage) Kind() string {
	return "helo"
}

func (m HeloMessage) Trace() string {
	return "<helo>"
}

type StartMessage struct {
	Jid  int      `json:"jid"`
	Argv []string `json:"argv"`
	Envp []string `json:"envp"`
	Dir  string   `json:"dir"`
}

func (m StartMessage) Receive(mqout chan SendableMessage, ptable ProcessTable, pg *sync.WaitGroup) bool {

	// log.Print("start", m.Jid)

	command := exec.Command(m.Argv[0], m.Argv[1:]...)
	command.Env = m.Envp
	command.Dir = m.Dir

	stdin, err := command.StdinPipe()

	if err != nil {
		log.Fatal(err)
	}

	command.Stdout = OutputForwarder{Mqout: mqout, Jid: m.Jid, Stream: STDOUT}
	command.Stderr = OutputForwarder{Mqout: mqout, Jid: m.Jid, Stream: STDERR}

	if err := command.Start(); err != nil {
		log.Fatal(err)
	}

	// m.Jid
	ptable[m.Jid] = Process{Command: command, Stdin: stdin}

	pg.Add(1)
	mqout <- StartedMessage{Jid: m.Jid, Pid: command.Process.Pid}

	go monitorProcess(mqout, pg, command, m.Jid)

	return true

}

type StartedMessage struct {
	Jid int `json:"jid"`
	Pid int `json:"pid"`
}

func (StartedMessage) Kind() string {
	return "started"
}

func (m StartedMessage) Trace() string {
	return fmt.Sprintf("<started Jid:%d>", m.Jid)
}

type StoppedMessage struct {
	Jid    int `json:"jid"`
	Pid    int `json:"pid"`
	Status int `json:"status"`
}

func (StoppedMessage) Kind() string {
	return "stopped"
}

func (m StoppedMessage) Trace() string {
	return fmt.Sprintf("<stopped Jid:%d Pid:%d>", m.Jid, m.Pid)
}

type OutputMessage struct {
	Jid     int    `json:"jid"`
	Stream  int    `json:"stream"`
	Sid     uint32 `json:"sid"`
	Payload []byte `json:"payload"`
}

func (OutputMessage) Kind() string {
	return "output"
}

func (m OutputMessage) Trace() string {
	return fmt.Sprintf("<output Jid:%d stream:%d sid:%d payload:%s",
		m.Jid, m.Stream, m.Sid, string(m.Payload))
}

type InputMessage struct {
	Jid     int    `json:"jid"`
	Iid     int    `json:"iid"`
	Payload []byte `json:"payload"`
}

func (m InputMessage) Receive(mqout chan SendableMessage, ptable ProcessTable, pg *sync.WaitGroup) bool {

	// log.Print("data", m.Jid)
	proc, exists := ptable[m.Jid]

	if exists {
		n, _ := proc.Stdin.Write(m.Payload)
		mqout <- ReceiptMessage{Jid: m.Jid, Iid: m.Iid, Size: n}
	}

	return true

}

type ReceiptMessage struct {
	Jid  int `json:"jid"`
	Iid  int `json:"iid"`
	Size int `json:"size"`
}

func (ReceiptMessage) Kind() string {
	return "receipt"
}

func (ReceiptMessage) Trace() string {
	return "<receipt>"
}

type EOFMessage struct {
	Jid int `json:"jid"`
}

func (m EOFMessage) Receive(mqout chan SendableMessage, ptable ProcessTable, pg *sync.WaitGroup) bool {

	// log.Print("got eof")
	proc, exists := ptable[m.Jid]

	if exists {
		proc.Stdin.Close()
	}
	// log.Print("closed stdin")

	return true

}

type ByeMessage struct {
}

func (m ByeMessage) Receive(mqout chan SendableMessage, ptable ProcessTable, pg *sync.WaitGroup) bool {

	return false

}
