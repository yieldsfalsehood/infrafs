#!/bin/bash

python3 -m venv ~/var/venv/infrafs
source ~/var/venv/infrafs/bin/activate
make GOPATH="${VIRTUAL_ENV}" install

##

# this is development process, not part of the infrafs tooling
# itself. update and commit the signatures when the config files
# change.

gpg --detach-sign --armor \
    cpace/files/kernel/linux.config

gpg --detach-sign --armor --yes \
    cpace/files/keyring/gnupg/0482D84022F52DF1C4E7CD43293ACD0907D9495A.key

gpg --detach-sign --armor --yes \
    cpace/files/keyring/gnupg/647F28654894E3BD457199BE38DBBDC86092693E.key

# self signed!
gpg --detach-sign --armor --yes \
    cpace/files/keyring/gnupg/C2551EF4670EF4DD821428DC6BF3FBDBD6B83C28.key

##

clusterdir="${PWD}/cluster/cluster1"

##

# infrafs prepare-keyring keyring

keyring="${clusterdir}/keyring/gnupg"

rm -rf "$keyring"
install -dm 0700 "$keyring"

#

gpg --homedir "$keyring" \
    --list-keys

#

bsdtar -cf - \
       @demo/keyring.mtree \
    | tar -xf - -C "$keyring"

gpg --homedir "$keyring" \
    --batch \
    --passphrase "" \
    --quick-gen-key \
    signer

# self signed on this side
gpg --homedir "$keyring" \
    --import "$keyring"/C2551EF4670EF4DD821428DC6BF3FBDBD6B83C28.key
gpg --homedir "$keyring" \
    --quick-lsign-key C2551EF4670EF4DD821428DC6BF3FBDBD6B83C28
gpg --homedir "$keyring" \
    --verify \
    "$keyring"/C2551EF4670EF4DD821428DC6BF3FBDBD6B83C28.key.asc \
    "$keyring"/C2551EF4670EF4DD821428DC6BF3FBDBD6B83C28.key

gpg --homedir "$keyring" \
    --verify \
    "$keyring"/0482D84022F52DF1C4E7CD43293ACD0907D9495A.key.asc \
    "$keyring"/0482D84022F52DF1C4E7CD43293ACD0907D9495A.key
gpg --homedir "$keyring" \
    --import "$keyring"/0482D84022F52DF1C4E7CD43293ACD0907D9495A.key
gpg --homedir "$keyring" \
    --quick-lsign-key 0482D84022F52DF1C4E7CD43293ACD0907D9495A

gpg --homedir "$keyring" \
    --verify \
    "$keyring"/647F28654894E3BD457199BE38DBBDC86092693E.key.asc \
    "$keyring"/647F28654894E3BD457199BE38DBBDC86092693E.key
gpg --homedir "$keyring" \
    --import "$keyring"/647F28654894E3BD457199BE38DBBDC86092693E.key
gpg --homedir "$keyring" \
    --quick-lsign-key 647F28654894E3BD457199BE38DBBDC86092693E

#

python -m cpace.script \
       cluster/cluster1 \
       demo \
       scripts.prepare-keyring \
       gnupg

python -m cpace.script \
       cluster/cluster1 \
       demo \
       scripts.prepare-keyring \
       gnupg \
    | execvm --local

#

cpace cluster/cluster1 \
      demo \
      scripts.prepare-keyring \
      gnupg

#

infrafs cluster/cluster1 \
        demo \
        prepare-keyring \
        gnupg...

##

# * implement infrafs download-rootfs alpine

os="${clusterdir}/os/alpine"

rm -rf "$os"
install -dm 0750 "$os"

curl -O --output-dir "$os" \
     https://dl-cdn.alpinelinux.org/alpine/v3.15/releases/x86_64/alpine-minirootfs-3.15.0-x86_64.tar.gz
curl -O --output-dir "$os" \
     https://dl-cdn.alpinelinux.org/alpine/v3.15/releases/x86_64/alpine-minirootfs-3.15.0-x86_64.tar.gz.asc

##

# infrafs prepare-pkg infrafs

pkg="${clusterdir}/pkg/infrafs"

GOPATH="$pkg" go clean
rm -rf "$pkg"
install -dm 0750 "$pkg"

make -C jsh GOPATH="$pkg" install
# GOPATH="$PWD"/go/go go install -v ./... # dev
# GOPATH="$PWD"/go/go go install -v gitlab.com/yieldsfalsehood/infrafs.git@dev # prod

gpg --homedir "$keyring" \
    --detach-sign --armor \
    "$pkg"/bin/jsh

##

# infrafs prepare-initrd initrd

initrd="${clusterdir}/initrd/initos"
rm -rf "$initrd"
install -dm 0750 "$initrd"

gpg --homedir "$keyring" \
    --verify \
    "$os"/alpine-minirootfs-3.15.0-x86_64.tar.gz.asc \
    "$os"/alpine-minirootfs-3.15.0-x86_64.tar.gz

gpg --homedir "$keyring" \
    --verify \
    "$pkg"/bin/jsh.asc \
    "$pkg"/bin/jsh

# cpace data initrd initrd | bsdtar @-
bsdtar -cf "$initrd"/initrd.cpio \
       --format cpio \
       @"$os"/alpine-minirootfs-3.15.0-x86_64.tar.gz \
       @demo/initrd.mtree

gpg --homedir "$keyring" \
    --detach-sign --armor \
    "$initrd"/initrd.cpio

##

# infrafs prepare-initos

initos="${clusterdir}/initos/initos"
rm -rf "$initos"
install -dm 0750 "$initos"

#

gpg --homedir "$keyring" \
    --verify \
    "$initrd"/initrd.cpio.asc \
    "$initrd"/initrd.cpio

# initos is its own installer, although the iso makes directories
# read-only...
bsdtar -cf - \
       @"$initrd"/initrd.cpio \
       @demo/initos.mtree \
    | tar -xf - -C "$initos"

env -i PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
    unshare --user --map-root-user \
        --root "$initos" \
        /bin/sh

env -i PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
    unshare --user --map-root-user \
        --root "$initos" \
        /bin/jsh

/sbin/apk add --no-chown gnupg gpg

# /sbin/apk add --no-chown libarchive-tools

/sbin/apk add --no-chown bash bison curl elfutils-dev findutils flex-dev gcc linux-headers make musl-dev ncurses-dev openssl openssl-dev perl

/sbin/apk add --no-chown go

# https://lunar.computer/posts/kvm-qemu-libvirt-alpine/
/sbin/apk add --no-chown qemu-img

# install each host's emulator
/sbin/apk add --no-chown qemu-system-x86_64

# install each host's volume's mkfs
/sbin/apk add --no-chown e2fsprogs

/usr/bin/gpg --homedir /etc/infrafs/keyring/gnupg \
             --list-keys

#

python -m cpace.script \
       cluster/cluster1 \
       demo \
       scripts.initos-install \
       initos \
    | execvm --local

python -m cpace.script \
       cluster/cluster1 \
       demo \
       scripts.initos-configure \
       initos \
    | execvm -m cpace.emulator \
             cluster/cluster1 \
             demo \
             emulators.unshare \
             initos

##

# infrafs download-kernel

env -i PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
    unshare --user --map-root-user \
        --root "$initos" \
        /bin/sh

curl -O --output-dir /usr/src \
     https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.15.11.tar.xz

unxz /usr/src/linux-5.15.11.tar.xz

curl -O --output-dir /usr/src \
     https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.15.11.tar.sign

##

# infrafs build-kernel

env -i PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
    unshare --user --map-root-user \
        --root "$initos" \
        /bin/sh

/usr/bin/gpg --homedir /etc/infrafs/keyring/gnupg \
             --verify \
             /etc/infrafs/kernel/linux/config.asc \
             /etc/infrafs/kernel/linux/config

/usr/bin/gpg --homedir /etc/infrafs/keyring/gnupg \
             --verify \
             /usr/src/linux-5.15.11.tar.sign \
             /usr/src/linux-5.15.11.tar

/bin/tar -xf /usr/src/linux-5.15.11.tar \
         -C /usr/src

/usr/bin/make -C /usr/src/linux/linux-5.15.11 mrproper

/usr/bin/make -C /usr/src/linux/linux-5.15.11 alldefconfig KCONFIG_ALLCONFIG=/etc/infrafs/kernel/linux/config

/usr/bin/make -C /usr/src/linux/linux-5.15.11 -j8

/usr/bin/make -C /usr/src/linux/linux-5.15.11 INSTALL_PATH=/var/lib/infrafs/kernel/linux install

##

# sign-kernel

gpg --homedir "$keyring" \
    --detach-sign --armor \
    "$initos"/var/lib/infrafs/kernel/linux/vmlinuz

##

env -i PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
    unshare --user --map-root-user \
        --root "$initos" \
        /bin/sh

/usr/bin/gpg --homedir /etc/infrafs/keyring/gnupg \
             --verify \
             /var/lib/infrafs/initrd/initos/initrd.cpio.asc \
             /var/lib/infrafs/initrd/initos/initrd.cpio

/usr/bin/gpg --homedir /etc/infrafs/keyring/gnupg \
             --verify \
             /var/lib/infrafs/kernel/linux/vmlinuz.asc \
             /var/lib/infrafs/kernel/linux/vmlinuz

##

final thoughts here before i step away:

prepare-keyring

download-rootfs
prepare-initrd # {verify rootfs ; build cpio}

# same as below re: kernel signing, the separate executions are
# convenient but have me wondering about how to encode synchronizing
# executions beyond just "run them in sequence". maybe a new type that
# wraps multiple executions? running an execution of that type causes
# each of its consituent executions to run in sequence. otherwise
# there is no top level sequencing - the execution model could switch
# to declaring a bunch of these things on a queue, then having
# potentially parallel workers pop them off and .run() them.
#
# maybe i'm circling back to doit after all..maybe instead of having
# to do the subprocess.run() myself i could return executions as
# python actions. in fact, the "infrafs <subcommand>" taxonomy seems
# out of line with the rest of the project - everywhere else these
# commands have been captured as per-cluster configuration data. maybe
# the cli should be `infrafs config.yaml... -- doit-args`, where i'd
# generate the tasks from those configs then defer to doit to select
# and run tasks based on the remainder args. also wondering if that
# task generator logic shouldn't be part of the cpace config, if i'm
# treating it as per-cluster. maybe there is one more "meta-config"
# file that assigns cpace scripts to objects and actions?
prepare-initos # {verify cpio ; extract}
install-initos # {install packages}
provision-initos # {verify cpio ; extract} ; {install packages}

# signing as a separate execution feels a little awkward because it
# means i have to encode running those two executions back to back. if
# another execution ran in between, for instance, that one might
# change the kernel and i'd then be signing the wrong thing.
download-kernel # {download ; uncompress}
build-kernel # {verify config source ; unpack ; configure ; compile ; install}
sign-kernel # {sign}

# now we should be able to `unshare qemu initrd kernel` in such a way
# that we get a /bin/sh for interactive development.

# pause here until we can also `unshare qemu initrd kernel` to get a
# jsh - in particular we need to populate /init in the initrd, so
# cpace config needs to fill that in now.

# also reflect on image encryption and key signing, e.g. should i sign
# the encryption key then check that signature before starting qemu?
# should i be generating an encryption key per host? i could
# e.g. generate one master key for the entire cluster way earlier than
# here, like when i initialize the keydir. that *could* be
# unprotected, but providing a passphrase should be possible.

qemu-img create
mke2fs
openssl rand
qemu-img convert

# install

##

# emulators

# should accept -f to add values.yamls
# and --set for individual keys
#
# cluster/cluster1 is output directory
# demo directory contains {values.yaml,files,templates,lua}
# emulators.jsh is a lua module that will be imported and run to
#   produce an emulator object
python -m cpace.emulator \
       cluster/cluster1 \
       demo \
       emulators.jsh

python -m cpace.emulator \
       cluster/cluster1 \
       demo \
       emulators.unshare \
       initos

python -m cpace.emulator \
       cluster/cluster1 \
       demo \
       emulators.chroot.emulator \
       workstation

##

python -m infrafs.initos.scripts.initrd {version} {arch}
python -m infrafs.initos.scripts.initrd {version} {arch} \
    | execvm -- jsh/jsh
infrafs -c conf/cluster1.yaml prepare-initos-initrd

python -m infrafs.initos.scripts.kernel {name} {version} {arch} {config}
python -m infrafs.initos.scripts.kernel linux 5.15.11 x86_64 initos.config \
    | execvm -e MAKEFLAGS=-j8 -- jsh/jsh
python -m infrafs.initos.scripts.kernel {version} {arch} \
    | execvm -- jsh/jsh
infrafs -c conf/cluster1.yaml prepare-initos-kernel

python -m infrafs.initos.scripts.emulator qemu initrd kernel memory
python -m infrafs.initos.scripts.emulator qemu initrd kernel memory \
    | execvm -- jsh
python -m infrafs.initos.initos qemu initrd kernel memory

python -m infrafs.installer {os}
python -m infrafs.installer {os} \
    | execvm -- jsh
infrafs -c conf/cluster1.yaml prepare-installer alpine

python -m infrafs.volume.clean {datadir} {hostname} {name}
python -m infrafs.volume.clean {datadir} {hostname} {name} \
    | execvm -- jsh
infrafs -c conf/cluster1.yaml clean-host-volumes workstation

python -m infrafs.volume.prepare {datadir} {hostname} {name} {uuid} {type} {size}
python -m infrafs.volume.prepare {datadir} {hostname} {name} {uuid} {type} {size} \
    | execvm -- jsh
infrafs -c conf/cluster1.yaml prepare-host-volumes workstation

python -m infrafs.initos.scripts.installer
execvm -i <(python -m infrafs.initos.scripts.installer) \
       -- python -m infrafs.initos.initos qemu initrd kernel memory
python -m infrafs.initos.installer qemu initrd kernel memory bootfs rootfs

python -m infrafs.install.os os bootfs root
python -m infrafs.install.os os bootfs root \
    | execvm -- python -m infrafs.initos.installer qemu initrd kernel memory bootfs rootfs
infrafs -c conf/cluster1.yaml install-os workstation

python -m infrafs.initos.scripts.host
execvm -i <(python -m infrafs.initos.scripts.host) \
       -- python -m infrafs.initos.initos qemu initrd kernel memory
python -m infrafs.initos.host qemu initrd kernel memory bootfs rootfs

cpace -c conf/cluster1.yaml data workstation \
    | tar -tzf -
python -m cpace.unpack
cpace -c conf/cluster1.yaml data workstation \
    | execvm -i <(python -m cpace.unpack) \
             -- python -m infrafs.initos.host qemu initrd kernel memory bootfs rootfs

cpace -c conf/cluster1.yaml control workstation
cpace control \
    | execvm -- python -m infrafs.initos.host qemu initrd kernel memory bootfs rootfs

# data|unpack ; control
infrafs -c conf/cluster1.yaml prepare workstation

##

python -m infrafs.install.bootloader
python -m infrafs.install.bootloader \
    | execvm -- python -m infrafs.host.chroot.emulator qemu initrd kernel memory bootfs rootfs
infrafs -c conf/cluster1.yaml install-bootloader workstation

python -m cpace.script \
       cluster/cluster1 \
       demo \
       emulators.chroot.script \
       workstation \
    | python -m cpace.emulator \
             cluster/cluster1 \
             demo \
             emulators.chroot.emulator \
             workstation

##

# prepare-host-volumes ; install ; prepare ; install-bootloader
infrafs -c conf/cluster1.yaml provision workstation

python -m infrafs.network.start
python -m infrafs.network.start \
    | execvm -- jsh
infrafs -c conf/cluster1.yaml start-network vlan1

python -m infrafs.network.stop
python -m infrafs.network.stop \
    | execvm -- jsh
infrafs -c conf/cluster1.yaml start workstation

python -m infrafs.host.attach workstation.sock

python -m infrafs.host.scripts.ssh {host}
execvm -i <(python -m infrafs.host.scripts.ssh {host}) \
       -- python -m infrafs.host.attach workstation.sock
python -m infrafs.host.ssh workstation.sock {host}

cpace -c conf/cluster1.yaml data workstation \
    | execvm -m cpace.unpack \
             -- python -m infrafs.ssh workstation.sock {host}
cpace control \
    | execvm -- python -m infrafs.ssh workstation.sock {host}
infrafs -c conf/cluster1.yaml configure workstation

infrafs -c conf/cluster1.yaml map workstation.sock {host}
