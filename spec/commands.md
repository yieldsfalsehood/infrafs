
# Derivations

To start I should be able to start and stop networks:

```
$ infrafs -d datadir
          -c config.yaml...
          start-network network...
```

```
$ infrafs -d datadir
          -c config.yaml...
          stop-network network...
```

create `{hostname}.{volume}.{img,key,qcow2}`

```
$ infrafs -d datadir
          -c config.yaml...
          prepare-host-volume hostname volume...
```

```
$ infrafs -d datadir
          -c config.yaml...
          clean-host-volume hostname volume...
```

Here I am explicit about the alpine installer emulator and alpine
install script. NOTE: I'm not passing data to the installer! The
install script lies: the /boot location really shouldn't come from
cpace, it's not a configuration item, it's an interface fixed by the
installer itself.

NEIN NEIN NEIN! INFRAFS SHOULD GENERATE EXECVM SCRIPTS THEN RUN THEM
WITH A LOCAL JSH EMULATOR!

In other words, the enforced pattern for all infrafs commands should
be that they simply generate a list of argvs that get wrapped into an
execvm script that is run here. That means there can be no special
stdin/out wrangling, and no knowledge of or interface with cpace!
Indeed, as the demo should work up doing these by hand, that should
start with an example that does a by-hand tar of a static directory!
And the configuration script should be a statically defined one
captured in a yaml doc. Most of that already exists! So now, I think I
will actually still wind up with a Makefile! infrafs will be the gluey
bits, then I'll stick with shell-isms for piping in/out of those
commands, for instance.

```
$ execvm exec -d datadir
              -c config.yaml...
              -s infrafs.emulators.installers.alpine
              -i install/alpine.yaml
              --
              hostname
```

The alpine specific bits in the emulator are the isos (the installer
and packages, for insance).

```
$ def alpine(datadir, config, hostname):
      return [
          "qemu",
          *alpine_cdroms(config),
          *qemu_nics(config, hostname),
          *qemu_volumes(config, hostname),
      ]
```

Since the emulator takes arguments, I could refactor that to a factory
function that dispatches based on something like
`config.hosts.{hostname}.os`. I could do similarly and refactor
`install/alpine.yaml` to a function
`infrafs.scripts.installers.alpine`. I'd have to pass the dispatch key
plus any installer-specific configuration items in environment
variables since script generators can't take parameters. That looks
like this:

```
$ INFRAFS_INSTALLER=alpine
  INFRAFS_ALPINE_ROOTFS=uuid
  INFRAFS_ALPINE_BOOTFS=uuid
  execvm exec -d datadir
              -c config.yaml...
              -s infrafs.emulators.installers
              -m infrafs.scripts.installers.alpine
              --
              hostname
```

The script generator would look something like:

```
def alpine():
    return shell.Script([
        shell.Command(
            argv=["mount", "installer.iso", "/opt"],
        ),
        # ...
        shell.Command(
            argv=["mount", os.environ["ROOTFS"], "/mnt"],
        ),
        # ...
    ])
```

That works and could be scripted into a Makefile for instance. It
requires parsing and repeating configuration several times when I feel
I'd rather pass along one single configuration object. So, rather than
let `execvm` build my script for me, `infrafs` may have his own
factory that can specialize the installer emulator/script combination
and call something like `infrafs.installers._alpine(config)`.

```
def _alpine(config):
    return shell.Script([
        shell.Command(
            argv=["mount", "installer.iso", "/opt"],
        ),
        # ...
        shell.Command(
            argv=["mount", config.host.volumes.root, "/mnt"],
        ),
        # ...
    ])
```

In a perfect world, both versions of that function would actually call
something else that parameterizes those variables.

```
$ infrafs -d datadir
          -c config.yaml...
          install hostname...
```

Finally, there should be a dependency check for the installer. First,
he should depend on the volumes so that they're created, formatted,
and encrypted. Then he may have a dynamic check that runs initos to
mount rootfs and check for the presence of `/etc/alpine-version`.

Once the installation is complete I need to perform an offline
configuration that makes the host bootable by populating network
configuration, enabling services, etc. This is similar to the
installation but changing out the emulator/script and passing data
input.

```
$ cpace data -c config.yaml... hostname
      | execvm exec -d datadir
                    -c config.yaml...
                    -s infrafs.emulators.controller
                    -i <(cpace control -c config.yaml... hostname)
                    --
                    hostname
```

The `controller` emulator also boots initos, but with the host volumes
and nics and no cdroms, i.e. there's no os-specialization:

```
$ def controller(datadir, config, hostname):
      return [
          "qemu",
          *qemu_nics(config, hostname),
          *qemu_volumes(config, hostname),
      ]
```

`cpace` needs to produce an entire configuration object, which means
he needs access to all the configuration objects. I think it makes
sense that each configuration overlay must be a separate document,
therefore it should make sense to concatenate them like this as a
hacky kinda way to do this by hand.

```
$ cpace data -c config.yaml... hostname
      | INFRAFS_CONFIG=<(cat config.yaml...)
        INFRAFS_HOSTNAME=hostname
        execvm exec -d datadir
                    -c config.yaml...
                    -s infrafs.emulators.controller
                    -m infrafs.scripts.configure
                    --
                    hostname
```

Then configure calls `cpace` to produce a script:

```
def configure():
    return cpace.load(config=os.environ["INFRAFS_CONFIG"],
                      hostname=os.environ["INFRAFS_HOSTNAME"])
```

Note that `cpace` should be prefixing its generated script with a `tar
-xzf - -C /` (which is good and makes sense since he's the one
producing the config tarball, so he can tie together the
formatting/compression there).

Finally I can produce `infrafs configure-offline`, which has access to
a higher scoped config object, which it can pass down to
`cpace`. Then, since the same `cpace` object is producing both the
config tarball and the script, I get nice guarantees that he's using
the same formatting/compression, for instance, whereas the "by
hand"/piping construction allows for inconsistencies like piping the
configuration tarball for one host into the emulator for another!

```
$ infrafs -d datadir
          -c config.yaml...
          configure-offline hostname...
```

I like the idea of "prepare" running a sequence of `install` then
`configure-offline` as a shortcut:

```
$ infrafs -d datadir
          -c config.yaml...
          prepare hostname...
```

Now I can start and stop hosts:

```
$ infrafs -d datadir
          -c config.yaml...
          start hostname...
```

```
$ infrafs -d datadir
          -c config.yaml...
          stop hostname...
```

Online configuration follows a similar process as offline
configuration, but using a different emulator.

```
$ cpace data -c config.yaml... hostname
      | execvm exec -d datadir
                    -c config.yaml...
                    -s infrafs.emulators.backdoor
                    -i <(cpace control -c config.yaml... hostname)
                    --
                    hostname
```

Rather than starting a host, the backdoor emulator would connect to
the backdoor socket and send a connect message. We could implement
that as a separate cli in `execvm` that takes a socket, for instance:

```
$ execvm connect {datadir}/{hostname}.backdoor
```

The emulator could take care of deriving the socket location:

```
def backdoor(datadir, config, hostname):
    return [
        "execvm", "connect", backdoor_socket(datadir, config, hostname),
    ]
```

Then tie it all together with a convenience api:

```
$ infrafs -d datadir
          -c config.yaml...
          configure hostname...
```

Presumably each host also opens a regular terminal console that we
might attach to, analogous to `execvm connect`. Note that this makes
for some awkwardness for how to coordinate all the other commands
through doit, whereas this one should stay attached to stdin, etc. and
isn't really a task.

```
$ infrafs -d datadir
          -c config.yaml...
          console hostname...
```

It makes me wonder if I shouldn't just do this without the config,
either, like maybe there's a community command that already does this
and I'm thinkin to do all that fussin just to sugar the socket
location?

```
$ xxx connect {datadir}/{hostname}.console
```

# Notes

For future derived work `datadir` may abide a convention like
`{cluster}/{environment}`

i think maybe the cpace entrypoint module should be specified as a
config item on the host
