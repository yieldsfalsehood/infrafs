#! /usr/bin/env python3

import argparse
import pathlib
import subprocess
import sys

import cpace.configuration

from . import install
from . import kernel
from . import network
from . import symbols
from . import volumes


def install_alpine():
    return [
        "execvm", "-s", "infrafs.emulators:installer",
        "-i", "scripts/install.yaml",
        "--",
        "--qemu", "qemu-system-x86_64",
        "--memory", "256M",
        "--kernel", "$kernel",
        "--kernel-log", "${ndqf}-install.log",
        "--initrd", "$initrd",
        "--cdrom1", "$packages",
        "--cdrom2", "$alpine",
        "--key", "$key",
        "--boot", "${bootvol}.qcow2",
        "--root", "${rootvol}.qcow2",
    ]


def parse_args(argv):

    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--datadir",
                        default=pathlib.Path.cwd(),
                        type=pathlib.PurePath)
    parser.add_argument("-c", "--config", action="append")

    subparsers = parser.add_subparsers(dest="command",
                                       help="command",
                                       required=True)

    kernel.parsers(subparsers)
    install.parsers(subparsers)
    network.parsers(subparsers)
    volumes.parsers(subparsers)

    return parser.parse_args(argv)


def dispatch(command):

    handlers = {
        **kernel.handlers(),
        **install.handlers(),
        **network.handlers(),
        **volumes.handlers(),
    }

    return handlers[command]


def run(argv):

    args = parse_args(argv)
    config = cpace.configuration.read(args.config)

    command = symbols.Command(args.command)
    handler = dispatch(command)

    for execution in handler(args, config):
        if execution.run():
            print("error running execution")
            break


def main():
    run(sys.argv[1:])
