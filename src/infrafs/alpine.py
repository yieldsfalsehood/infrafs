#!/usr/bin/env python3

import hashlib
import itertools

from execvm import shell

from . import volumes


PACKAGES = [
    "bash",
    "eudev",
    "linux-lts",
    "openssh",
    "sudo",
    "syslinux",
]

SERVICES = [
    # custom services
    ("sshd", "boot"),
    # base services, per alpine-make-vm-image
    ("bootmisc", "boot"),
    ("cgroups", "sysinit"),
    ("devfs", "sysinit"),
    ("dmesg", "sysinit"),
    ("hostname", "boot"),
    ("hwclock", "boot"),
    ("hwdrivers", "sysinit"),
    ("modules", "boot"),
    ("mount-ro", "shutdown"),
    ("networking", "boot"),
    ("savecache", "shutdown"),
    ("swap", "boot"),
    ("sysctl", "boot"),
    ("syslog", "boot"),
]


def serialize(*args, **kwargs):
    return ",".join(
        itertools.chain(
            args,
            ("{k}={v}".format(k=k, v=v) for k, v in kwargs.items())
        )
    )


def chardev(id, *args, **kwargs):
    return [
        "-chardev", serialize(*args, id=id, **kwargs),
        "-device", serialize("virtconsole", chardev=id, name=id),
    ]


def cdrom(fname):
    return [
        "-drive", serialize(media="cdrom", file=fname),
    ]


def drive(volume):
    return [
        "-object", serialize("secret",
                             id=volume.id,
                             file=volume.keyname),
        "-drive", serialize(
            file=volume.volumename,
            format="qcow2",
            **{
                "if": "virtio",
                "encrypt.key-secret": volume.id,
            }
        ),
    ]


def drives(datadir, hostname, config):
    return itertools.chain.from_iterable(
        drive(volumes.Volume(datadir, hostname, name,
                             config.volumes[name]))
        for name in config.volumes
    )


def qemu(arch):
    return f"qemu-system-{arch}"


def emulator(datadir, hostname, config):
    cdrom1 = "alpine/alpine-packages-3.14.0-x86_64.iso"
    cdrom2 = "alpine/alpine-extended-3.14.0-x86_64.iso"
    return shell.Emulator([
        qemu(config.arch),

        "-no-reboot",
        "-nographic",

        "-nic", "none",

        # installer configuration

        "-kernel", "linux/linux-5.12.14-x86_64.bzImage",
        "-initrd", "initos/initos.cpio",

        "-monitor", "none",
        "-serial", "none",

        "-device", "virtio-serial",
        *chardev("hvc0", "file", path="kernel.log"),
        *chardev("hvc1", "stdio"),

        "-append", "console=hvc0 -- /dev/hvc1",

        *cdrom(cdrom1),
        *cdrom(cdrom2),

        # host-specific configuration

        "-machine", serialize(type="pc", accel="kvm"),
        "-m", config.memory,

        *drives(datadir, hostname, config),
    ])


def rc_add(service, level):
    return shell.Command(
        argv=["chroot", "/mnt",
              "rc-update", "add", service, level],
    )


def md5sum(*args):

    md5 = hashlib.md5()

    for arg in args:
        md5.update(arg)

    return md5.hexdigest()


def install(config):

    rootfs = config.volumes["root"].uuid
    bootfs = config.volumes["boot"].uuid

    packagesiso = md5sum(b"alpine", b"3.14.0", b"x86_64")[:16]
    installeriso = "alpine-ext 3.14.0 x86_64"

    return shell.Script([
        # mount the install media
        shell.Command(
            argv=["mount",
                  "-t", "iso9660",
                  "-o", "ro",
                  f"LABEL={packagesiso}", "/opt"],
        ),
        shell.Command(
            argv=["mount",
                  "-t", "iso9660",
                  "-o", "ro",
                  f"LABEL={installeriso}", "/opt/opt"],
        ),
        # prepare the base fs. alpine-base sets up the filesystem
        # layout plus some basic config, into which i propagate the
        # standard mounts.
        shell.Command(
            argv=["mount", f"UUID={rootfs}", "/mnt"],
        ),
        shell.Command(
            argv=["install", "-dm", "0755", "/mnt/boot"],
        ),
        shell.Command(
            argv=["mount", f"UUID={bootfs}", "/mnt/boot"],
        ),
        shell.Command(
            argv=["apk", "add",
                  "--repositories-file", "/opt/etc/apk/repositories",
                  "--keys-dir", "/opt/etc/apk/keys",
                  "--no-cache",
                  "--no-network",
                  "--root", "/mnt",
                  "--initdb",
                  "alpine-base"],
        ),
        shell.Command(
            argv=["mount", "-t", "devtmpfs", "dev", "/mnt/dev"],
        ),
        shell.Command(
            argv=["mount", "-t", "proc", "proc", "/mnt/proc"],
        ),
        shell.Command(
            argv=["mount", "-t", "sysfs", "sys", "/mnt/sys"],
        ),
        shell.Command(
            argv=["apk", "add",
                  "--repositories-file", "/opt/etc/apk/repositories",
                  "--keys-dir", "/opt/etc/apk/keys",
                  "--no-cache",
                  "--no-network",
                  "--root", "/mnt",
                  *PACKAGES],
        ),
        # "install" jsh
        shell.Command(
            argv=["install",
                  "/var/cache/initos.tar.gz",
                  "/mnt/var/cache/"],
        ),
        shell.Command(
            argv=["chroot", "/mnt",
                  "tar", "-xvzf", "/var/cache/initos.tar.gz",
                  "-C", "/"],
        ),
        # finally we can install the boot loader and enable some
        # services
        shell.Command(
            argv=["chroot", "/mnt",
                  "extlinux", "--install", "--reset-adv",
                  "/boot"],
        ),
        shell.Command(
            argv=["chroot", "/mnt",
                  "setup-udev", "-n"],
        ),
        *(rc_add(*service) for service in SERVICES),
        shell.Command(
            argv=["echo", "syncing"],
        ),
        shell.Command(
            argv=["sync"],
        ),
        shell.Command(
            argv=["echo", "install completed successfully"],
        ),
        shell.Command(
            argv=["poweroff", "-f"],
        ),
    ])
