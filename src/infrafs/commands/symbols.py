#!/usr/bin/env python3

import enum


class Command(enum.Enum):
    CHROOT = "chroot"
    INSTALL = "install"
    MOUNT = "mount"
