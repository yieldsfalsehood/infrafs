#!/usr/bin/env python3

import functools
import itertools

from . import symbols

import execvm.shell


def parsers(subparsers):

    parser = subparsers.add_parser(
        symbols.Command.MOUNT.value,
    )

    parser.add_argument("device")
    parser.add_argument("mountpoint")


def mount(device, mountpoint):
    return [
        execvm.shell.Command([
            "mount", device, mountpoint,
        ]),
    ]


def handler(args):
    return mount(args.device, args.mountpoint)


def handlers():
    return {
        symbols.Command.MOUNT: handler,
    }
