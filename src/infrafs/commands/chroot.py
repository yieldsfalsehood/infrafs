#!/usr/bin/env python3

import argparse
import functools
import itertools

from . import symbols

import execvm.shell


def parsers(subparsers):

    parser = subparsers.add_parser(
        symbols.Command.CHROOT.value,
    )

    parser.add_argument("--stdin", action="store_true")

    parser.add_argument("newroot")
    parser.add_argument("argv", nargs=argparse.REMAINDER)


def chroot(stdin, newroot, argv):
    return [
        execvm.shell.Command(
            ["chroot", newroot, *argv],
            stdin="stdin" if stdin else None
        ),
    ]


def handler(args):
    return chroot(args.stdin, args.newroot, args.argv)


def handlers():
    return {
        symbols.Command.CHROOT: handler,
    }
