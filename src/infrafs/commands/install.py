#!/usr/bin/env python3

import functools
import importlib.resources
import itertools

from . import mount
from . import symbols

import execvm.shell
import infrafs.scripts


def load(fname):
    files = importlib.resources.files(infrafs.scripts)
    body = files.joinpath(fname).read_bytes()
    return execvm.shell.Command.load_all(body)


def parsers(subparsers):

    parser = subparsers.add_parser(
        symbols.Command.INSTALL.value,
    )

    parser.add_argument("os")
    parser.add_argument("bootfs")
    parser.add_argument("rootfs")


def script_for_installer_chroot():
    return [
        # ifup -a

        *network.network("eth0", "10.0.2.2",
                         "10.0.2.3", "255.255.255.0"),

        *mount.mount("/dev/sr0", "/opt",
                     type="iso9660",
                     options=["ro"]),
        *mount.mount("dev", "/opt/dev", type="devtmpfs")
        *mount.mount("proc", "/opt/proc", type="proc")
        *mount.mount("sys", "/opt/sys", type="sysfs")

        *chroot.chroot(True, "/opt", ["jsh"]),
    ]


def handler(args):
    return control.Execution(
        shell.Emulator(
            initos.initos(args.qemu,
                          args.kernel_log,
                          args.initrd, args.kernel,
                          args.memory,
                          args.cdrom,
                          args.drive,
                          args.object),
        ),
        shell.Script(
            False,
            [],
            script_for_installer_chroot(),
        ),
    )

def script_for_guest_chroot():
    return [

        *mount.mount(f"UUID={rootfs}", "/mnt"),
        *mount.mount(f"UUID={bootfs}", "/mnt/boot"),

        *mount.mount("dev", "/mnt/dev", type="devtmpfs")
        *mount.mount("proc", "/mnt/proc", type="proc")
        *mount.mount("sys", "/mnt/sys", type="sysfs")

        *chroot.chroot(True, "/mnt", ["jsh"]),
    ]

def script(os, bootfs, rootfs):
    return [
        *mount.mount(f"UUID={rootfs}", "/mnt"),
        *mkdir.mkdir("/mnt/boot"),
        *mount.mount(f"UUID={bootfs}", "/mnt/boot"),
        *load(f"install/{os}.yaml"),
        *load("sync.yaml"),
    ]


def handler(args):
    return script(args.os, args.bootfs, args.rootfs)


def handlers():
    return {
        symbols.Command.INSTALL: handler,
    }
