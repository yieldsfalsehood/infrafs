#! /usr/bin/env python3

import argparse
import pathlib
import subprocess
import sys

from . import chroot
from . import install
from . import mount
from . import symbols


def parse_args(argv):

    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="command",
                                       help="command",
                                       required=True)

    chroot.parsers(subparsers)
    install.parsers(subparsers)
    mount.parsers(subparsers)

    return parser.parse_args(argv)


def dispatch(command):

    handlers = {
        **chroot.handlers(),
        **install.handlers(),
        **mount.handlers(),
    }

    return handlers[command]


def serialize(commands):
    for command in commands:
        print(command.dump())


def run(argv):

    args = parse_args(argv)

    command = symbols.Command(args.command)
    handler = dispatch(command)

    serialize(handler(args))


def main():
    run(sys.argv[1:])
