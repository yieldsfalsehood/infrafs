#!/usr/bin/env python3

import enum


class Command(enum.Enum):
    PREPARE_INITOS_KERNEL = "prepare-initos-kernel"
    START_NETWORK = "start-network"
    STOP_NETWORK = "stop-network"
    PREPARE_HOST_VOLUME = "prepare-host-volume"
    CLEAN_HOST_VOLUME = "clean-host-volume"
    INSTALL_HOST = "install"
    PREPARE_HOST = "prepare"
