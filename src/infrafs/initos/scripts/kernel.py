#!/usr/bin/env python3

import argparse
import functools
import itertools
import pathlib
import sys

import execvm.shell

# BOO
REPO = pathlib.PurePath("https://cdn.kernel.org/pub/linux/kernel")


def parse_args(argv):

    parser = argparse.ArgumentParser()

    parser.add_argument("name")
    parser.add_argument("version")
    parser.add_argument("arch")
    parser.add_argument("config")

    return parser.parse_args(argv)


def script(name, version, arch, config):

    major_version = version.split(".")[0]

    sname = f"linux-{version}"
    source = f"{sname}.tar.xz"
    url = REPO / f"v{major_version}.x" / source
    outdir = pathlib.PurePath("infrafs") / name
    alldefconfig = outdir / "alldef.config"
    kconfig = f"KCONFIG_ALLCONFIG={alldefconfig}"
    out = f"O={outdir}"

    return [
        execvm.shell.Command(
            ["curl", "--fail", "-O", str(url)]
        ),
        execvm.shell.Command(
            ["tar", "-xJf", source]
        ),
        execvm.shell.Command(
            ["make", "-C", sname, "mrproper"]
        ),
        execvm.shell.Command(
            ["make", "-C", sname, out, "mrproper"]
        ),
        execvm.shell.Command(
            ["cp", config, str(sname / alldefconfig)]
        ),
        execvm.shell.Command(
            ["make", "-C", sname, out, kconfig, "alldefconfig"]
        ),
        execvm.shell.Command(
            ["make", "-C", sname, out]
        ),
    ]


def serialize(commands):
    for command in commands:
        print(command.dump())


def main():
    args = parse_args(sys.argv[1:])
    commands = script(args.name, args.version, args.arch, args.config)
    serialize(commands)


if __name__ == "__main__":
    main()
