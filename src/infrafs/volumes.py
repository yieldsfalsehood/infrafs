#!/usr/bin/env python3

import functools

from execvm import control
from execvm import shell

from . import emulators
from . import symbols

# in bytes
VOLUME_ENCRYPT_KEY_LENGTH = "32"


def parsers(subparsers):

    parser_prepare = subparsers.add_parser(
        symbols.Command.PREPARE_HOST_VOLUME.value,
        help="create volume(s)",
    )
    parser_prepare.add_argument("hostname")
    parser_prepare.add_argument("volume", nargs="*")

    parser_clean = subparsers.add_parser(
        symbols.Command.CLEAN_HOST_VOLUME.value,
        help="clean volume(s)",
    )
    parser_clean.add_argument("hostname")
    parser_clean.add_argument("volume", nargs="*")


class Volume:

    def __init__(self, datadir, hostname, name, config):
        self.datadir = datadir
        self.hostname = hostname
        self.name = name
        self.config = config

    @property
    def id(self):
        return f"{self.hostname}.{self.name}"

    @property
    def prefix(self):
        return self.datadir / self.id

    @property
    def basename(self):
        return f"{self.prefix}.img"

    @property
    def keyname(self):
        return f"{self.prefix}.key"

    @property
    def volumename(self):
        return f"{self.prefix}.qcow2"

    def base(self):
        return shell.Command([
            "qemu-img", "create",
            "-f", "raw",
            self.basename,
            self.config.size,
        ])

    def mke2fs(self):
        return shell.Command([
            "mke2fs", "-F", "-t", "ext4",
            "-U", self.config.uuid, self.basename,
            self.config.size//1024,
        ])

    def key(self):
        return shell.Command([
            "openssl", "rand", "-base64",
            "-out", self.keyname,
            VOLUME_ENCRYPT_KEY_LENGTH,
        ])

    def encrypt(self):
        return shell.Command([
            "qemu-img", "convert",
            "-f", "raw",
            "-O", "qcow2",
            "--object", f'secret,id=sec0,file={self.keyname}',
            "-o", "encrypt.format=luks",
            "-o", "encrypt.key-secret=sec0",
            self.basename, self.volumename,
        ])

    def clean(self):
        return shell.Command([
            "rm", "-f",
            self.basename,
            self.keyname,
            self.volumename,
        ])


def execution(action, args, config, hostname, name):
    return control.Execution(
        emulators.jsh(),
        shell.Script(
            False,
            [],
            action(
                Volume(args.datadir, hostname, name,
                       config.hosts[hostname].volumes[name])
            )
        )
    )


def fmap(action, args, config):

    hostname = args.hostname

    return map(
        functools.partial(execution, action, args, config, hostname),
        args.volume or config.hosts[hostname].volumes.keys()
    )


def handler(action):
    return functools.partial(fmap, action)


def prepare(volume):
    return [
        volume.base(),
        volume.mke2fs(),
        volume.key(),
        volume.encrypt(),
    ]


def clean(volume):
    return [
        volume.clean(),
    ]


def handlers():
    return {
        symbols.Command.PREPARE_HOST_VOLUME: handler(prepare),
        symbols.Command.CLEAN_HOST_VOLUME: handler(clean),
    }
