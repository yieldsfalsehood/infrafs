#!/usr/bin/env python3

import argparse

from execvm import shell


def jsh():
    return shell.Emulator(["jsh/jsh"])


# def initos(argv):

#     parser = argparse.ArgumentParser()

#     parser.add_argument("--qemu")
#     parser.add_argument("--memory")
#     parser.add_argument("--kernel")
#     parser.add_argument("--kernel-log")
#     parser.add_argument("--initrd")

#     args = parser.parse_args(argv)

#     return shell.Emulator([
#         "qemu-system-x86_64",
#         "-machine", "type=pc,accel=kvm",
#         "-m", args.memory,
#         "-nographic",
#         "-no-reboot",
#         "-monitor", "none",
#         "-serial", f"file:{args.kernel_log}",
#         "-device", "virtio-serial",
#         "-chardev", "stdio,id=char0",
#         "-device", "virtconsole,chardev=char0",
#         "-nic", "none",
#         "-kernel", args.kernel,
#         "-initrd", args.initrd,
#         "-append", "debug console=ttyS0 -- /dev/hvc0",
#         "-virtfs", "local,path=data,mount_tag=data,security_model=none,readonly",
#     ])


# def installer(argv):

#     parser = argparse.ArgumentParser()

#     parser.add_argument("--qemu")
#     parser.add_argument("--memory")
#     parser.add_argument("--kernel")
#     parser.add_argument("--kernel-log")
#     parser.add_argument("--initrd")
#     parser.add_argument("--cdrom1")
#     parser.add_argument("--cdrom2")
#     parser.add_argument("--key")
#     parser.add_argument("--boot")
#     parser.add_argument("--root")

#     args = parser.parse_args(argv)

#     return shell.Emulator([
#         "qemu-system-x86_64",
#         "-machine", "type=pc,accel=kvm",
#         "-m", args.memory,
#         "-nographic",
#         "-no-reboot",
#         "-monitor", "none",
#         "-serial", f"file:{args.kernel_log}",
#         "-chardev", "stdio,id=virtiocon0",
#         "-device", "virtio-serial",
#         "-device", "virtconsole,chardev=virtiocon0",
#         "-nic", "none",
#         "-kernel", args.kernel,
#         "-initrd", args.initrd,
#         "-append", "debug console=ttyS0 -- /dev/hvc0",
#         "--drive", f"media=cdrom,file={args.cdrom1}",
#         "--drive", f"media=cdrom,file={args.cdrom2}",
#         "--object", f"secret,id=sec0,file={args.key}",
#         "-drive", f"file={args.boot},format=qcow2,if=virtio,encrypt.key-secret=sec0",
#         "-drive", f"file={args.root},format=qcow2,if=virtio,encrypt.key-secret=sec0",
#     ])


# def runner(argv):

#     parser = argparse.ArgumentParser()

#     parser.add_argument("--qemu")
#     parser.add_argument("--memory")
#     parser.add_argument("--kernel-log")
#     parser.add_argument("--key")
#     parser.add_argument("--boot")
#     parser.add_argument("--root")

#     args = parser.parse_args(argv)

#     return shell.Emulator([
#         "qemu-system-x86_64",
#         "-machine", "type=pc,accel=kvm",
#         "-m", args.memory,
#         "-nographic",
#         "-no-reboot",
#         "-monitor", "none",
#         "-serial", f"file:{args.kernel_log}",
#         "-device", "virtio-serial",
#         "-chardev", "stdio,id=char0",
#         "-device", "virtconsole,chardev=char0",
#         "-nic", "user,mac=ba:be:01:00:01:01",
#         "--object", f"secret,id=sec0,file={args.key}",
#         "-drive", f"file={args.boot},format=qcow2,if=virtio,encrypt.key-secret=sec0",
#         "-drive", f"file={args.root},format=qcow2,if=virtio,encrypt.key-secret=sec0",
#     ])
