#! /usr/bin/env python3

import argparse
import pathlib
import sys

from . import chroot
from . import initos
from . import symbols


def parse_args(argv):

    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="command",
                                       help="command",
                                       required=True)

    chroot.parsers(subparsers)
    initos.parsers(subparsers)

    return parser.parse_args(argv)


def dispatch(command):

    handlers = {
        **chroot.handlers(),
        **initos.handlers(),
    }

    return handlers[command]


def run(argv):

    args = parse_args(argv)

    command = symbols.Command(args.command)
    handler = dispatch(command)

    execution = handler(args)
    execution.run()


def main():
    run(sys.argv[1:])
