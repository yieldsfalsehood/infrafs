#!/usr/bin/env python3

import itertools

from execvm import control
from execvm import shell

from . import initos
from . import symbols

from ..commands import chroot
from ..commands import install
from ..commands import mount

def parsers(subparsers):

    parser = subparsers.add_parser(
        symbols.Command.CHROOT.value,
        help="chroot emulator",
    )

    parser.add_argument("qemu")
    parser.add_argument("initrd")
    parser.add_argument("kernel")
    parser.add_argument("memory")
    parser.add_argument("bootfs")
    parser.add_argument("rootfs")
    parser.add_argument("--kernel-log", default="none")
    parser.add_argument("--cdrom", action="append", default=[])
    parser.add_argument("--drive", action="append", default=[])
    parser.add_argument("--object", action="append", default=[])


def explode(flag, values):
    return itertools.chain.from_iterable(
        [flag, value] for value in values
    )


def bootstrap(bootfs, rootfs):
    return [
        *install.load("network.yaml"),
        *mount.mount(f"UUID={rootfs}", "/mnt"),
        *mount.mount(f"UUID={bootfs}", "/mnt/boot"),
        *chroot.chroot(True, "/mnt", ["jsh"]),
    ]


def handler(args):
    return control.Execution(
        shell.Emulator(
            initos.initos(args.qemu,
                          args.kernel_log,
                          args.initrd, args.kernel,
                          args.memory,
                          args.cdrom,
                          args.drive,
                          args.object),
        ),
        shell.Script(
            False,
            [],
            bootstrap(args.bootfs, args.rootfs),
        ),
    )


def handlers():
    return {
        symbols.Command.CHROOT: handler,
    }
