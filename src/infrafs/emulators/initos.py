#!/usr/bin/env python3

import itertools
import subprocess
import sys

from . import symbols


class SubprocessExecution:

    def __init__(self, args):
        self.args = args

    def run(self):
        return subprocess.run(self.args, stdin=sys.stdin)


def parsers(subparsers):

    parser = subparsers.add_parser(
        symbols.Command.INITOS.value,
        help="initos emulator",
    )

    parser.add_argument("qemu")
    parser.add_argument("initrd")
    parser.add_argument("kernel")
    parser.add_argument("memory")
    parser.add_argument("--kernel-log", default="none")
    parser.add_argument("--cdrom", action="append", default=[])
    parser.add_argument("--drive", action="append", default=[])
    parser.add_argument("--object", action="append", default=[])


def explode(flag, values):
    return itertools.chain.from_iterable(
        [flag, value] for value in values
    )


def serialize(*args, **kwargs):
    return ",".join(
        itertools.chain(
            args,
            ("{k}={v}".format(k=k, v=v) for k, v in kwargs.items())
        )
    )


def chardev(id, *args, **kwargs):
    return [
        "-chardev", serialize(*args, id=id, **kwargs),
        "-device", serialize("virtconsole", chardev=id, name=id),
    ]


def initos(qemu, kernel_log, initrd, kernel, memory, cdrom, drive, object):
    return [
        qemu,
        "-machine", "type=pc,accel=kvm",
        "-nographic",
        "-no-reboot",
        "-monitor", "none",
        "-serial", "none",
        "-device", "virtio-serial",
        *chardev("hvc0", "file", path=kernel_log),
        *chardev("hvc1", "stdio"),
        *chardev("elliot1", "file", path="elliot.log"),
        "-nic", "user,model=e1000,mac=10:22:33:44:55:66",
        "-append", "debug console=hvc0 -- /dev/hvc1",
        "-initrd", initrd,
        "-kernel", kernel,
        "-m", memory,
        *explode("-cdrom", cdrom),
        *explode("-drive", drive),
        *explode("-object", object),
    ]


def handler(args):
    return SubprocessExecution(initos(args.qemu,
                                      args.kernel_log,
                                      args.initrd, args.kernel,
                                      args.memory,
                                      args.cdrom,
                                      args.drive,
                                      args.object))


def handlers():
    return {
        symbols.Command.INITOS: handler,
    }
