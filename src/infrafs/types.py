#!/usr/bin/env python3

import abc


class KindedType(abc.ABC):

    @classmethod
    @abc.abstractmethod
    def kind():
        ...


class KindedDispatch(abc.ABCMeta):

    @property
    def types(cls):
        return {
            subclass.kind(): subclass
            for subclass in cls.__subclasses__()
        }

    def __getitem__(cls, arg):
        # i *think* this is more about pylint getting confused, but i
        # might be screwing with the types a little too much here. i'm
        # not sure what it thinks is the type of `cls.types`, but
        # changing its signature to a function for instance doesn't do
        # much but make things more confusing. so, i'll sort this when
        # it comes time to fix the types, for now i'll ignore as
        # things work.
        #
        # pylint: disable=unsubscriptable-object
        return cls.types[arg]
