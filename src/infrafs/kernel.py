#!/usr/bin/env python3

import functools

from execvm import control
from execvm import shell

from . import emulators
from . import symbols

from .initos.scripts import kernel


def parsers(subparsers):

    parser_start = subparsers.add_parser(
        symbols.Command.PREPARE_INITOS_KERNEL.value,
        help="prepare an initos initrd",
    )
    parser_start.add_argument("initos", nargs="*")


def execution(action, args, config, initos):
    return control.Execution(
        emulators.jsh(),
        shell.Script(
            False,
            ["MAKEFLAGS=-j8"],
            action(initos)
        )
    )


def fmap(action, args, config):
    return map(
        functools.partial(execution, action, args, config),
        config.initoses
    )


def handler(action):
    return functools.partial(fmap, action)


def script(initos):
    return kernel.script(
        initos["name"],
        initos["linux"]["version"],
        initos["linux"]["arch"],
        initos["linux"]["config"],
    )


def handlers():
    return {
        symbols.Command.PREPARE_INITOS_KERNEL: handler(script),
    }
