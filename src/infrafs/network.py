#!/usr/bin/env python3

import abc
import functools
import pathlib

from execvm import control
from execvm import shell

from . import emulators
from . import symbols
from . import types


def parsers(subparsers):

    parser_start = subparsers.add_parser(
        symbols.Command.START_NETWORK.value,
        help="start a network",
    )
    parser_start.add_argument("network", nargs="*")

    parser_stop = subparsers.add_parser(
        symbols.Command.STOP_NETWORK.value,
        help="stop a network",
    )
    parser_stop.add_argument("network", nargs="*")


class INetwork(types.KindedType):

    @classmethod
    @abc.abstractmethod
    def new(cls, datadir, name, config):
        ...


class Network(INetwork, metaclass=types.KindedDispatch):

    def __init__(self, datadir, name):
        self.datadir = datadir
        self.name = name

    @classmethod
    def dispatch(cls, datadir, name, config):

        try:
            factory = cls.types[config.kind]
        except KeyError:
            raise TypeError("bad kind") from None

        return factory.new(datadir, name, config)


class HostNetwork(Network):

    @classmethod
    def kind(cls):
        return "host"

    @classmethod
    def new(cls, datadir, name, config):
        return cls(datadir, name)

    def start(self):
        return []

    def stop(self):
        return []


class VdeNetwork(Network):

    @classmethod
    def kind(cls):
        return "vde"

    @classmethod
    def new(cls, datadir, name, config):
        return cls(datadir, name)

    @property
    def sock(self):
        return self.datadir / pathlib.PurePath(self.name)

    @property
    def mgmt(self):
        return self.sock / "mgmt"

    @property
    def pidfile(self):
        return self.datadir / f"{self.name}.pid"

    def start(self):
        return shell.Command([
            "vde_switch",
            "--daemon",
            "--sock", self.sock,
            "--mgmt", self.mgmt,
            "--pidfile", self.pidfile,
        ])

    def stop(self):
        return shell.Command([
            "pkill", "-F", self.pidfile,
        ])


def execution(action, args, config, name):
    return control.Execution(
        emulators.jsh(),
        shell.Script(
            action(
                Network.dispatch(
                    args.datadir,
                    name,
                    config.networks[name]
                )
            )
        )
    )


def fmap(action, args, config):
    return map(
        functools.partial(execution, action, args, config),
        args.network or config.networks.keys()
    )


def handler(action):
    return functools.partial(fmap, action)


def start(network):
    return [
        network.start(),
    ]


def stop(network):
    return [
        network.stop(),
    ]


def handlers():
    return {
        symbols.Command.START_NETWORK: handler(start),
        symbols.Command.STOP_NETWORK: handler(stop),
    }
