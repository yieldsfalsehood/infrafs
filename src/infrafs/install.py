#!/usr/bin/env python3

import abc
import asyncio
import contextlib
import functools
import itertools
import os
import sys

from execvm import control
from execvm import shell
from execvm import utils

from . import commands
from . import emulators
from . import symbols
from . import types
from . import volumes

import cpace.main


def serialize(*args, **kwargs):
    return ",".join(
        itertools.chain(
            args,
            ("{k}={v}".format(k=k, v=v) for k, v in kwargs.items())
        )
    )


def cdrom(fname):
    return [
        "--drive", serialize(media="cdrom", file=fname),
    ]


def drive(volume):
    return [
        "--object", serialize("secret",
                              id=volume.id,
                              file=volume.keyname),
        "--drive", serialize(
            file=volume.volumename,
            format="qcow2",
            **{
                "if": "virtio",
                "encrypt.key-secret": volume.id,
            }
        ),
    ]


def drives(host):
    return itertools.chain.from_iterable(
        drive(volumes.Volume(host.datadir, host.hostname, name,
                             host.host.volumes[name]))
        for name in host.host.volumes
    )


def parsers(subparsers):

    parser_install = subparsers.add_parser(
        symbols.Command.INSTALL_HOST.value,
        help="install host",
    )
    parser_install.add_argument("hostname", nargs="*")

    parser_prepare = subparsers.add_parser(
        symbols.Command.PREPARE_HOST.value,
        help="prepare host",
    )
    parser_prepare.add_argument("hostname", nargs="*")


class IInstaller(types.KindedType):

    @classmethod
    @abc.abstractmethod
    def new(cls, datadir, name, config):
        ...


class Installer(IInstaller, metaclass=types.KindedDispatch):

    def __init__(self, datadir, hostname, config):
        self.datadir = datadir
        self.hostname = hostname
        self.config = config

    @classmethod
    def dispatch(cls, datadir, hostname, config):

        try:
            factory = cls.types[config.kind]
        except KeyError:
            raise TypeError("bad kind") from None

        return factory.new(datadir, hostname, config)


class AlpineInstaller(Installer):

    @classmethod
    def kind(cls):
        return "alpine"

    @classmethod
    def new(cls, datadir, hostname, config):
        return cls(datadir, hostname, config)

    def emulator(self):
        return alpine.emulator(self.datadir,
                               self.hostname,
                               self.config)

    def script(self):
        return alpine.install(self.config)


class Host:

    def __init__(self, datadir, hostname, config):

        self.datadir = datadir
        self.hostname = hostname
        self.cluster = config
        self.host = config.hosts[hostname]


class Pipe:

    def __init__(self):

        fd1, fd2 = os.pipe()

        self.read = os.fdopen(fd1, "rb")
        self.write = os.fdopen(fd2, "wb")


class CpaceInput:

    def __init__(self, host):
        self.host = host

    def run(self, fout):
        cpace.main.run(self.host.cluster, self.host.hostname,
                       "cpace.workstation", fout)
        fout.close()

    @contextlib.asynccontextmanager
    async def open(self, loop=None):
        pipe = Pipe()
        await asyncio.to_thread(self.run, pipe.write)
        reader = await control.open_non_file_stream_reader(pipe.read, loop or asyncio.get_event_loop())
        yield utils.AIterReader(reader)


def execution(action, args, config, hostname):
    return action(Host(args.datadir, hostname, config))


def fmap(action, args, config):
    return map(
        functools.partial(execution, action, args, config),
        args.hostname or config.hosts.keys()
    )


def handler(action):
    return functools.partial(fmap, action)


def install(host):

    os = "alpine" # host.config.os
    bootfs = host.host.volumes["boot"].uuid
    rootfs = host.host.volumes["root"].uuid

    emulator = shell.Emulator([
        "infrafs-emu", "initos",
        "qemu-system-x86_64",
        "initos/initos.cpio",
        "initos/linux/vmlinuz",
        host.host.memory,
        "--kernel-log", "file:install.log",
        *cdrom("alpine-minirootfs-3.14.3-x86_64.iso"),
        *drives(host),
    ])

    return control.Execution(
        emulator,
        shell.Script(
            False,
            [],
            commands.install.script(os, bootfs, rootfs)
        )
    )


def prepare(host):

    os = "alpine" # host.config.os
    bootfs = host.host.volumes["boot"].uuid
    rootfs = host.host.volumes["root"].uuid

    emulator = shell.Emulator([
        "infrafs-emu", "chroot",
        "qemu-system-x86_64",
        "initos/initos.cpio",
        "initos/linux/vmlinuz",
        host.host.memory,
        bootfs,
        rootfs,
        "--kernel-log", "file:prepare.log",
        *drives(host),
    ])

    return control.Execution(
        emulator,
        shell.Script(
            False,
            [],
            [
                shell.Command(["tar", "-xvzf", "-", "-C", "/"],
                              stdin="stdin"),
            ],
        ),
        stdin=CpaceInput(host)
    )


def handlers():
    return {
        symbols.Command.INSTALL_HOST: handler(install),
        symbols.Command.PREPARE_HOST: handler(prepare),
    }
