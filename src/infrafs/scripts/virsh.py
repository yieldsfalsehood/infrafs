#!/usr/bin/env python3

import os

from execvm import shell


def packages():
    requirements = ["iptables", "dnsmasq"]
    return shell.Script([
        shell.Command(
            argv=["apk", "add", *requirements],
        ),
        shell.Command(
            argv=["poweroff", "-f"],
        ),
    ])


def rcadd(svc):
    return shell.Command(
        argv=["/sbin/rc-update", "add", *svc]
    )


def services():
    requirements = [
        ("iptables", "default"),
        ("dnsmasq", "default"),
    ]
    return shell.Script([
        *map(rcadd, requirements),
        shell.Command(
            argv=["poweroff", "-f"],
        ),
    ])


def vol_create_as(uri, pool, name, format):
    return shell.Command(
        argv=[
            "virsh", "-c", uri,
            "vol-create-as",
            pool, name,
            "0",
            "--format", format,
        ]
    )


def vol_upload(uri, pool, name, dir=None, target=None):
    return shell.Command(
        argv=[
            "virsh", "-c", uri,
            "vol-upload",
            "--pool", pool,
            target or name, name
        ],
        dir=dir,
    )


def virsh_upload(uri, pool, name, format, dir=None):
    return [
        vol_create_as(uri, pool, name, format),
        vol_upload(uri, pool, dir, name),
    ]


def provision():

    hostname = os.environ["INFRAFS_PROVISION_HOSTNAME"]
    uri = os.environ["INFRAFS_PROVISION_URI"]
    pool = os.environ["INFRAFS_PROVISION_POOL"]

    key = f"{hostname}.key"
    bootvol = f"{hostname}-boot.qcow2"
    rootvol = f"{hostname}-root.qcow2"

    return shell.Script([
        *virsh_upload(uri, pool, key, "raw"),
        *virsh_upload(uri, pool, bootvol, "qcow2"),
        *virsh_upload(uri, pool, rootvol, "qcow2"),
        shell.Command(
            argv=["poweroff", "-f"],
        ),
    ])
