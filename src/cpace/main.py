#!/usr/bin/env python3

import argparse
import sys

import lupa
from lupa import LuaRuntime

from . import configuration
from . import contents
from . import filesystem
from . import tarchive


def parse_args(argv):

    parser = argparse.ArgumentParser()

    parser.add_argument("config")
    parser.add_argument("hostname")

    return parser.parse_args(argv)


def run(config, hostname, fout):

    lua = LuaRuntime(unpack_returned_tuples=True)
    mname = config.hosts[hostname].cpace.entry
    module, _ = lua.require(mname)

    c = {
        "hostname": hostname,
        "cluster": config,
        "host": config.hosts[hostname],
    }

    fs = filesystem.Filesystem()
    templater = contents.Templater()

    # the execution model here is that i run the lua code to modify
    # the state of `fs`, then iterate through the items populated in
    # `fs` to push output to stdout.
    #
    # i originally tried returning a "list" of file/dir objects from
    # lua rather than modifying `fs` state, but i was struggling with
    # how to deal with nested lua calls that each want to define file
    # obects. i could have flattened, but that looked a little akward.
    #
    # i'm currently doing io during lua processing, which might be
    # where i want to target. the lua code can modify `fs` state by
    # pushing file _definitions_ into it, but then a separate computer
    # process should be evaluating those definitions into tarchive
    # format.

    module({
        "c": c,
        "fs": fs,
        "read": contents.Read,
        "buffer": contents.StringBuffer,
        "template": templater,
    })

    tarchive.tarchive(fs.files, fout)


def main():

    args = parse_args(sys.argv[1:])
    config = configuration.read([args.config])

    run(config, args.hostname, sys.stdout.buffer)
