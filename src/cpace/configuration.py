#!/usr/bin/env python3

import functools

import config


def read(fnames):
    return config.ConfigurationSet(
        *map(
            functools.partial(
                config.config_from_yaml,
                read_from_file=True
            ),
            fnames
        )
    )


def load(fname, hostname):

    conf = read([fname])

    return {
        "hostname": hostname,
        "cluster": conf,
        "host": conf.hosts[hostname],
    }
