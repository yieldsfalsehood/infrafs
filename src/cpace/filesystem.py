#!/usr/bin/env python3

import collections
import functools


def factory(receiver, constructor, name, kwargs):
    return receiver(constructor(name, **kwargs))


class Resource:

    def __init__(self, name, user, group, mode, mtime):
        self.name = name
        self.user = user
        self.group = group
        self.mode = int(mode, 8)
        self.mtime = mtime


class Directory(Resource):

    def __init__(self, name, user="root", group="root",
                 mode="0755", mtime=0):
        super().__init__(name, user, group, mode, mtime)

    def __repr__(self):
        return f"Directory({self.name}, user={self.user}, group={self.group}, mode={self.mode:o})"


class File(Resource):

    def __init__(self, name, user="root", group="root",
                 mode="0644", mtime=0, content=None):
        super().__init__(name, user, group, mode, mtime)
        self.content = content

    def __repr__(self):
        return f"File({self.name}, user={self.user}, group={self.group}, mode={self.mode:o})"


class Filesystem:

    def __init__(self):
        self.files = collections.deque()

    def _receive(self, resource):
        self.files.append(resource)
        return resource

    def directory(self, name):
        return functools.partial(
            factory, self._receive, Directory, name
        )

    def file(self, name):
        return functools.partial(
            factory, self._receive, File, name
        )
