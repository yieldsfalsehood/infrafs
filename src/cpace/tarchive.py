#!/usr/bin/env python3

import functools
import io
import pathlib
import tarfile

from . import filesystem


def relative(path):
    if path.is_absolute():
        return path.relative_to("/")
    return path


def tarinfo(type, resource):

    name = relative(pathlib.PurePath(resource.name))

    tarinfo = tarfile.TarInfo(str(name))
    tarinfo.type = type

    tarinfo.uname = resource.user
    tarinfo.gname = resource.group
    tarinfo.mode = resource.mode
    tarinfo.mtime = resource.mtime

    return tarinfo


def add_file(tar, file):

    info = tarinfo(tarfile.REGTYPE, file)

    content = bytes(file.content)
    fileobj = io.BytesIO(content)
    fileobj.seek(0)

    info.size = len(content)

    tar.addfile(info, fileobj)


def add_directory(tar, dir):
    info = tarinfo(tarfile.DIRTYPE, dir)
    tar.addfile(info, None)


def transformer(transformers, resource):
    for types, transformer in transformers.items():
        if isinstance(resource, types):
            return transformer(resource)


def tarchive(files, fileobj):

    with tarfile.open(fileobj=fileobj, mode="w|") as tar:
        transformers = {
            filesystem.File: functools.partial(add_file, tar),
            filesystem.Directory: functools.partial(add_directory,
                                                    tar),
        }
        transform = functools.partial(transformer, transformers)
        for file in files:
            transform(file)
