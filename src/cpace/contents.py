#!/usr/bin/env python3

import collections
import functools
import itertools

import jinja2


def factory(constructor, env, name, context):
    return constructor(env, name, context)


class Resource:
    pass


class Read(Resource):

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f"Read({self.name})"

    def __bytes__(self):
        with open(self.name, "rb") as fin:
            return fin.read()


class StringBuffer(Resource):

    def __init__(self, contents):
        self.buffer = collections.deque(contents.values())

    def __repr__(self):
        return f"Buffer()"

    def append(self, content):
        self.buffer.append(content)

    def __str__(self):
        return "".join(str(chunk) for chunk in self.buffer)

    def __bytes__(self):
        return str(self).encode()


class Template(Resource):

    def __init__(self, env, name, context):
        self.env = env
        self.name = name
        self.context = context

    def __repr__(self):
        return f"TemplateInput({self.name})"

    def __str__(self):
        template = self.env.get_template(self.name)
        return template.render(self.context)

    def __bytes__(self):
        return str(self).encode()


class Templater:

    def __init__(self):
        self.env = jinja2.Environment(
            loader=jinja2.FileSystemLoader("."),
        )

    def __call__(self, name):
        return functools.partial(
            factory, Template, self.env, name
        )
