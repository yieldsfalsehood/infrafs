#!/usr/bin/env python3

import argparse
import collections.abc
import pathlib
import sys

import lupa
from lupa import LuaRuntime

from execvm import shell


import yaml


def parse_args(argv):

    parser = argparse.ArgumentParser()

    parser.add_argument("configdir",
                        type=pathlib.PurePath)
    parser.add_argument("mname")
    parser.add_argument("args", nargs="*")

    return parser.parse_args(argv)


def executioner(argv):
    print("exec:", list(argv.values()), file=sys.stderr)


def path(*args):
    return pathlib.PosixPath(*args)


def command(table):
    argv = map(str, table.argv.values())
    return shell.Command(argv)


class Tar:

    def __call__(self, table):
        entries = list(table.entries.values())
        target = table.target
        return shell.Tarchive(entries, target)

    def file(self, table):
        name = table.name
        contents = table.contents
        return shell.TarchiveFile(name, contents)


def file(table):
    name = table.name
    return shell.File(name)


def table_from_helper(value):
        return table_from(value)


def table_from(lua, values):

    if isinstance(values, collections.abc.Mapping):
        return lua.table_from({
            table_from(lua, k): table_from(lua, v)
            for k, v in values.items()
        })

    elif isinstance(values, (list, tuple)):
        return lua.table_from([
            table_from(lua, v)
            for v in values
        ])

    return values


def application(luapath, mname, args, values):

    lua = LuaRuntime(unpack_returned_tuples=True)

    g = lua.globals()
    g.package.path = luapath
    g.package.loaded.execvm = {
        "path": path,
        "command": command,
        "tar": Tar(),
        "file": file,
    }

    module, _ = lua.require(mname)

    # '/usr/local/share/lua/5.4/?.lua;/usr/local/share/lua/5.4/?/init.lua;/usr/local/lib/lua/5.4/?.lua;/usr/local/lib/lua/5.4/?/init.lua;./?.lua;./?/init.lua'
    return module.coroutine(table_from(lua, args),
                            table_from(lua, values))


def main():

    args = parse_args(sys.argv[1:])

    luapath = str(args.configdir / "lua" / "?.lua")

    fname = str(args.configdir / "cluster.yaml")

    with open(fname) as fin:
        values = yaml.safe_load(fin)

    for statement in application(luapath, args.mname,
                                 args.args, values):
        print(statement.dump())


if __name__ == "__main__":
    main()
