#!/usr/bin/env python3

import abc
import asyncio
import base64
import collections
import datetime
import itertools
import json
import sys

from . import shim
from . import utils


PROTOCOL_VERSION = "1.0"

STDOUT = 1
STDERR = 2


class Process:

    def __init__(self, stdin, stdout, stderr):
        self.stdin = stdin
        self.output = {
            STDOUT: stdout,
            STDERR: stderr,
        }
        self.stopped = asyncio.get_event_loop().create_future()
        self.iqueue = {}
        self.forwarder = None


class Encodable(abc.ABC):

    @classmethod
    @abc.abstractmethod
    def __getstate__(cls):
        ...


class Message(Encodable):

    @classmethod
    @abc.abstractmethod
    def kind(cls):
        ...


class StartMessage(Message):

    def __init__(self, jid, argv, envp, dir):
        self._jid = jid
        self._argv = argv
        self._envp = envp
        self._dir = dir

    @classmethod
    def kind(cls):
        return "start"

    def __getstate__(self):
        return {
            "jid": self._jid,
            "argv": self._argv,
            "envp": self._envp,
            "dir": self._dir,
        }


class InputMessage(Message):

    def __init__(self, jid, iid, payload):
        self._jid = jid
        self._iid = iid
        self._payload = payload

    @classmethod
    def kind(cls):
        return "input"

    def __getstate__(self):
        return {
            "jid": self._jid,
            "iid": self._iid,
            "payload": base64.b64encode(self._payload).decode(),
        }


class EofMessage(Message):

    def __init__(self, jid):
        self._jid = jid

    @classmethod
    def kind(cls):
        return "eof"

    def __getstate__(self):
        return {
            "jid": self._jid,
        }


class ByeMessage(Message):

    def __init__(self):
        pass

    @classmethod
    def kind(cls):
        return "bye"

    def __getstate__(self):
        return {}


class Envelope(Encodable):

    def __init__(self, message):
        self._message = message

    def __getstate__(self):
        return {
            "version": PROTOCOL_VERSION,
            "kind": self._message.kind(),
            "params": self._message.__getstate__(),
        }


class Transport:

    def __init__(self, istream, ostream):
        self._istream = istream
        self._ostream = ostream

    async def read(self):
        line = await self._istream.readline()
        if line:
            return json.loads(line)
        return None

    def __aiter__(self):
        return utils.AIter(self.read)

    async def send(self, message):
        envelope = Envelope(message)
        encoded = json.dumps(envelope.__getstate__()).encode()
        return self._ostream.write(encoded)

    def close(self):
        self._ostream.close()


class ReceivableMessageMeta(abc.ABCMeta):

    @property
    def types(cls):
        return {
            subclass.kind(): subclass
            for subclass in cls.__subclasses__()
        }

    def __getitem__(cls, arg):
        # i *think* this is more about pylint getting confused, but i
        # might be screwing with the types a little too much here. i'm
        # not sure what it thinks is the type of `cls.types`, but
        # changint its signature to a function for instance doesn't do
        # much but make things more confusing. so, i'll sort this when
        # it comes time to fix the types, for now i'll ignore as
        # things work.
        #
        # pylint: disable=unsubscriptable-object
        return cls.types[arg]


class ReceivableMessage(Message, metaclass=ReceivableMessageMeta):

    @classmethod
    def new(cls, doc):

        try:
            version = doc["version"]
            kind = doc["kind"]
            params = doc["params"]
        except KeyError:
            raise TypeError("bad format") from None

        if version != PROTOCOL_VERSION:
            raise TypeError("protocol version mismatch") from None

        try:
            factory = cls.types[kind]
        except KeyError:
            raise TypeError("bad kind") from None

        return factory.new(params)

    @abc.abstractmethod
    async def receive(self, protocol):
        ...


class Protocol:

    def __init__(self, transport, script, status,
                 istreams, ostreams):

        self._transport = transport
        self._script = script
        self._status = status

        self._istreams = istreams
        self._ostreams = ostreams

        self._jid = itertools.count()
        self._iid = itertools.count()
        self._process_table = {}

    async def _execute(self, command):

        stdin = self._istreams.get(command.stdin)
        stdout = self._ostreams.get(command.stdout)
        stderr = self._ostreams.get(command.stderr)

        jid = next(self._jid)
        self._process_table[jid] = Process(stdin, stdout, stderr)

        # print("starting", command.argv, file=sys.stderr)

        message = StartMessage(
            jid,
            command.argv,
            command.envp,
            command.dir,
        )
        await self._transport.send(message)

        status = await self._process_table[jid].stopped
        del self._process_table[jid]

        return status

    async def _executioner(self):

        for command in self._script:

            # print(" ".join(command.argv), file=sys.stderr)
            status = await self._execute(command)

            self._status.append(status)

            if status:
                break

        await self._transport.send(ByeMessage())

        self._transport.close()

    async def _forward(self, jid, chunk):

        # t1 = datetime.datetime.now()

        iid = next(self._iid)
        future = asyncio.get_event_loop().create_future()
        self._process_table[jid].iqueue[iid] = future

        await self._transport.send(InputMessage(jid, iid, chunk))

        # t2 = datetime.datetime.now()

        size = await future
        del self._process_table[jid].iqueue[iid]

        # t3 = datetime.datetime.now()

        # print("chunk sent", t2-t1, t3-t2)

        # size might be <0 in case of full buffer for instance
        return chunk[max(size, 0):]

    async def _forwarder(self, jid):

        # print(datetime.datetime.now(), "forwarding", file=sys.stderr)
        stdin = self._process_table[jid].stdin

        if stdin:
            async with stdin.open() as reader:
                async for chunk in reader:
                    # print("forwarding chunk", file=sys.stderr)
                    while chunk := await self._forward(jid, chunk):
                        pass

            # print(datetime.datetime.now(), "sending eof",
            #       file=sys.stderr)
            await self._transport.send(EofMessage(jid))

    async def helo(self):
        asyncio.create_task(self._executioner())

    async def started(self, jid):
        task = asyncio.create_task(self._forwarder(jid))
        self._process_table[jid].forwarder = task

    async def output(self, jid, stream, payload):

        try:
            proc = self._process_table[jid]
        except KeyError:
            # log that we got an unknown jid
            pass
        else:

            try:
                stream = proc.output[stream]
            except KeyError:
                # log that we got data for an unknown stream
                pass
            else:
                if stream:
                    # print("recv", payload, file=sys.stderr)
                    stream.write(shim.Shammy(payload))
                    await stream.drain()

    async def receipt(self, jid, iid, size):

        # print("receipt", jid, size)

        try:
            proc = self._process_table[jid]
        except KeyError:
            # log that we got an unknown jid
            print("OH NO")
        else:

            try:
                future = proc.iqueue[iid]
            except KeyError:
                # log that we got a receipt we weren't expecting
                print("OH NO")
                # pass
            else:
                future.set_result(size)

    async def stopped(self, jid, status):
        # print("stopped", jid, status)
        try:
            proc = self._process_table[jid]
        except KeyError:
            # log that we got a stop for a proc we don't know about
            pass
        else:
            proc.stopped.set_result(status)

    async def execv(argv, stdout, stderr):
        process_table[jid] = Process(stdout, stderr)
        await send(start-message(argv))
        await started()
        return process_table[jid]


class HeloMessage(ReceivableMessage):

    @classmethod
    def kind(cls):
        return "helo"

    def __getstate__(self):
        return {}

    @classmethod
    def new(cls, doc):
        return cls()

    async def receive(self, protocol):
        await protocol.helo()


class StartedMessage(ReceivableMessage):

    def __init__(self, jid):
        self._jid = jid

    @classmethod
    def kind(cls):
        return "started"

    def __getstate__(self):
        return {
            "jid": self._jid,
        }

    @classmethod
    def new(cls, doc):
        return cls(doc["jid"])

    async def receive(self, protocol):
        await protocol.started(self._jid)


class OutputMessage(ReceivableMessage):

    def __init__(self, jid, stream, payload):
        self._jid = jid
        self._stream = stream
        self._payload = payload

    @classmethod
    def kind(cls):
        return "output"

    def __getstate__(self):
        return {
            "jid": self._jid,
            "stream": self._stream,
            "payload": base64.b64encode(self._payload),
        }

    @classmethod
    def new(cls, doc):
        return cls(doc["jid"],
                   doc["stream"],
                   base64.b64decode(doc["payload"]))

    async def receive(self, protocol):
        await protocol.output(self._jid, self._stream, self._payload)


class ReceiptMessage(ReceivableMessage):

    def __init__(self, jid, iid, size):
        self._jid = jid
        self._iid = iid
        self._size = size

    @classmethod
    def kind(cls):
        return "receipt"

    def __getstate__(self):
        return {
            "jid": self._jid,
            "iid": self._iid,
            "size": self._size,
        }

    @classmethod
    def new(cls, doc):
        return cls(doc["jid"], doc["iid"], doc["size"])

    async def receive(self, protocol):
        await protocol.receipt(self._jid, self._iid, self._size)


class StoppedMessage(ReceivableMessage):

    def __init__(self, jid, status):
        self._jid = jid
        self._status = status

    @classmethod
    def kind(cls):
        return "stopped"

    def __getstate__(self):
        return {
            "jid": self._jid,
            "status": self._status,
        }

    @classmethod
    def new(cls, doc):
        return cls(doc["jid"], doc["status"])

    async def receive(self, protocol):
        await protocol.stopped(self._jid, self._status)


class Server:

    def __init__(self, transport, script, istreams, ostreams):
        self._transport = transport
        self._status = collections.deque()
        self._protocol = Protocol(transport, script, self._status,
                                  istreams, ostreams)

    async def _step(self, doc):

        try:
            message = ReceivableMessage.new(doc)
        except TypeError:
            # log invalid input
            pass
        else:
            await message.receive(self._protocol)

    async def run(self):

        async for doc in self._transport:
            await self._step(doc)

        # the status of the server will be the last exit code
        # returned, or 0 if no process ran
        return (self._status or [0])[-1]


async def create_jsh(argv):

    proc = await asyncio.create_subprocess_exec(
        *argv,
        stderr=sys.stderr,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )

    transport = Transport(proc.stdout, proc.stdin)
    protocol = Protocol(transport)

    return transport, protocol


async def open_jsh(argv):

    transport, protocol = await create_jsh(argv)
    server = Server(transport, protocol)

    return server
