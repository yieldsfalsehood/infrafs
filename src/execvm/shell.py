#!/usr/bin/env python3

import asyncio
import asyncio.subprocess
import base64
import collections
import contextlib
import io
import itertools
import logging
import os
import stat
import subprocess
import tarfile
import weakref

import jinja2

import yaml

logger = logging.getLogger("execvm")


PATH = ":".join([
    "/usr/local/sbin",
    "/usr/local/bin",
    "/usr/sbin",
    "/usr/bin",
    "/sbin",
    "/bin",
])

ENVP = [
    f"PATH={PATH}",
]


class YAMLObject(yaml.YAMLObject):

    yaml_loader = yaml.SafeLoader
    yaml_dumper = yaml.SafeDumper

    def dump(self):
        return yaml.safe_dump(self, explicit_start=True)

    @classmethod
    def load(cls, stream):
        return yaml.safe_load(stream)

    @classmethod
    def load_all(cls, stream):
        return yaml.safe_load_all(stream)

    @classmethod
    def from_yaml(cls, loader, node):
        state = loader.construct_mapping(node, deep=True)
        return cls(**state)


class NullProcess:

    def write(self, data):
        pass

    def close(self):
        pass

    async def wait(self):
        pass


class NullEmulator:

    yaml_tag = u"!null-emulator"

    @contextlib.asynccontextmanager
    async def run(self):
        yield self

    @contextlib.asynccontextmanager
    async def execv(self, argv, stdout, stderr):
        yield NullProcess()


class SubprocessProcess:

    def __init__(self, transport, protocol, loop):
        self.transport = transport
        self.protocol = protocol
        self.loop = loop

    @property
    def pid(self):
        return self.transport.get_pid()

    @property
    def stdin(self):
        return self.transport.get_pipe_transport(0)

    async def wait(self):
        """Wait until the process exit and return the process return code."""
        return await self.transport._wait()

    def write(self, data):
        self.stdin.write(data)

    async def flush(self):
        self.stdin.flush()

    def close(self):
        self.stdin.close()


class SubprocessProtocol(asyncio.SubprocessProtocol):

    def __init__(self, stdout, stderr):
        self.stdout = stdout
        self.stderr = stderr

    def pipe_data_received(self, fd, data):
        if fd == 1:
            self.stdout.write(data)
        elif fd == 2:
            self.stderr.write(data)


class SubprocessProtocolFactory:

    def __init__(self, stdout, stderr):
        self.stdout = stdout
        self.stderr = stderr

    def __call__(self):
        return SubprocessProtocol(self.stdout, self.stderr)


class SubprocessEmulator:

    yaml_tag = u"!subprocess-emulator"

    @contextlib.asynccontextmanager
    async def run(self):
        yield self

    @contextlib.asynccontextmanager
    async def execv(self, argv, stdout, stderr):

        loop = asyncio.get_event_loop()

        transport, protocol = await loop.subprocess_exec(
            SubprocessProtocolFactory(stdout, stderr),
            *argv,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        process = SubprocessProcess(transport, protocol, loop)

        yield process

        process.close()
        await process.wait()


class JshProcess:

    def write(self, data):
        send(data-message(data))
        receipt()

    def close(self):
        pass

    async def wait_closed(self):
        await send(eof-message())
        await self.stopped

    async def __aexit__(self):
        self.close()
        await self.wait_closed()


class JshExecutioner():
    @contextlib.asynccontextmanager
    async def execv(self, command, stdout, stderr):
        await send(start())
        await started()
        process = JshProcess()
        yield process


async def session(self, proc):
    await helo()
    yield JshExecutioner()
    await send(bye())


class JshEmulator(YAMLObject):

    yaml_tag = u"!jsh-emulator"

    def __init__(self, argv):
        self.argv = list(map(str, argv))

    async def run(self):
        proc = await Proc()
        async with session(proc) as executioner:
            yield executioner
        await proc.wait()


class Variable(YAMLObject):

    yaml_tag = u"!env"

    def __init__(self, name, default=""):
        self.name = str(name)
        self.default = str(default)

    def __str__(self):
        return os.environ.get(self.name, self.default)


class Command(YAMLObject):

    yaml_tag = u"!command"

    def __init__(self, argv, envp=None, dir="",
                 stdout=None, stderr=None):
        self.argv = list(map(str, argv))
        self.envp = list(map(str, envp)) if envp else []
        self.dir = str(dir)
        self.stdout = stdout or Fd(1)
        self.stderr = stderr or Fd(2)

    @contextlib.asynccontextmanager
    async def start(self, emulator):
        async with self.stdout.start(emulator) as stdout:
            async with self.stderr.start(emulator) as stderr:
                logger.debug("execv(%s)", ", ".join(self.argv))
                async with emulator.execv(self.argv,
                                          stdout, stderr) as process:
                    yield process

    async def run(self, emulator):
        async with self.start(emulator) as process:
            pass


class Stream:

    def __init__(self, transport):
        self.transport = transport

    def write(self, data):
        self.transport.write(data)

    async def flush(self):
        if hasattr(self.transport, "flush"):
            task = self.transport.flush()
            if task:
                await task

    async def read(self, size):
        loop = asyncio.get_event_loop()
        data = await loop.run_in_executor(None,
                                          self.transport.read,
                                          size)
        return data

    def close(self):
        self.transport.close()

    async def blocks(self, size=asyncio.streams._DEFAULT_LIMIT):
        while data := await self.read(size):
            yield data

    async def readall(self):
        buffer = collections.deque()
        async for block in self.blocks():
            buffer.append(block)
        return b"".join(buffer)

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.flush()
        self.close()


class BufferedWriter:

    def __init__(self, transport):
        self.transport = transport
        self.buffer = collections.deque()
        self.task = None
        self.finalizer = None

    def write(self, data):

        self.buffer.append(data)

        if self.task and self.task.done():
            self.finalizer()

        if not self.task or (self.task and self.task.done()):
            self.task = asyncio.create_task(self.forward())
            self.finalizer = weakref.finalize(self, self.task.result)

    def close(self):
        pass

    async def forward(self):

        while self.buffer:
            data = self.buffer.popleft()
            loop = asyncio.get_event_loop()
            await loop.run_in_executor(None,
                                       self.transport.write, data)

    async def flush(self):

        if self.task:
            try:
                await self.task
            finally:
                self.task = None
                self.finalizer.detach()


class Fd(YAMLObject):

    yaml_tag = u"!fd"

    def __init__(self, fd):
        self.fd = fd

    @contextlib.asynccontextmanager
    async def open(self, emulator):
        async with Stream(self) as stream:
            yield stream

    def close(self):
        pass

    @contextlib.asynccontextmanager
    async def start(self, emulator):
        async with Stream(BufferedWriter(self)) as stream:
            yield stream

    def read(self, size):
        return os.read(self.fd, size)

    def write(self, data):
        return os.write(self.fd, data)


class Null(YAMLObject):

    yaml_tag = u"!null"

    @contextlib.asynccontextmanager
    async def open(self, emulator):
        yield self

    @contextlib.asynccontextmanager
    async def start(self, emulator):
        yield self

    async def read(self, size):
        return None

    def write(self, data):
        pass

    def close(self):
        pass

    async def flush(self):
        pass


class FileTransport:

    def __init__(self, handle):
        self.handle = handle

    def read(self, size):
        return self.handle.read(size)

    def write(self, data):
        return self.handle.write(data)

    def close(self):
        return self.handle.close()

    async def flush(self):
        pass


class File(YAMLObject):

    yaml_tag = u"!file"

    def __init__(self, name):
        self.name = str(name)

    @contextlib.asynccontextmanager
    async def open(self, emulator):
        transport = FileTransport(open(self.name, "rb"))
        async with Stream(transport) as stream:
            yield stream

    @contextlib.asynccontextmanager
    async def start(self, emulator):
        transport = FileTransport(open(self.name, "wb"))
        writer = BufferedWriter(transport)
        async with Stream(writer) as stream:
            yield stream


class Cat(YAMLObject):

    yaml_tag = u"!cat"

    def __init__(self, sources=None, target=None,
                 size=asyncio.streams._DEFAULT_LIMIT):
        self.sources = sources or []
        self.target = target or Fd(1)
        self.size = size

    async def run(self, emulator):
        async with self.target.start(emulator) as fout:
            for source in self.sources:
                async with source.open(emulator) as fin:
                    async for block in fin.blocks(self.size):
                        fout.write(block)
                        await fout.flush()


class Tarchive(YAMLObject):

    yaml_tag = u"!tar"

    def __init__(self, entries, target=None):
        self.entries = entries
        self.target = target or Fd(1)

    async def run(self, emulator):
        async with self.target.start(emulator) as fout:
            with tarfile.open(fileobj=fout, mode="w|") as tarchive:
                for entry in self.entries:
                    await entry.add(emulator, tarchive)


class TarchiveFile(YAMLObject):

    yaml_tag = u"!tar/file"

    def __init__(self, name, contents):
        self.name = str(name)
        self.contents = contents

    def tarinfo(self, contents):

        tarinfo = tarfile.TarInfo(self.name)
        tarinfo.type = tarfile.REGTYPE
        tarinfo.size = len(contents)

        # tarinfo.uname = self.user
        # tarinfo.gname = self.group
        # tarinfo.mode = self.mode
        # tarinfo.mtime = self.mtime

        return tarinfo

    async def add(self, emulator, tarchive):

        async with self.contents.open(emulator) as fin:

            contents = await fin.readall()
            tarinfo = self.tarinfo(contents)

            fileobj = io.BytesIO(contents)
            fileobj.seek(0)

            tarchive.addfile(tarinfo, fileobj)


class TarchiveDirectory(YAMLObject):

    yaml_tag = u"!tar/dir"

    def __init__(self, name):
        self.name = name

    def tarinfo(self):

        tarinfo = tarfile.TarInfo(self.name)
        tarinfo.type = tarfile.DIRTYPE

        # tarinfo.uname = self.user
        # tarinfo.gname = self.group
        # tarinfo.mode = self.mode
        # tarinfo.mtime = self.mtime

        return tarinfo

    async def add(self, emulator, tarchive):
        tarinfo = self.tarinfo()
        tarchive.addfile(tarinfo, None)


class String(YAMLObject):

    yaml_tag = u"!string"

    def __init__(self, value, encoding="utf-8"):
        self.value = value
        self.encoding = encoding

    @contextlib.asynccontextmanager
    async def open(self, emulator):
        transport = io.BytesIO(self.value.encode(self.encoding))
        async with Stream(transport) as stream:
            yield stream


class Base64(YAMLObject):

    yaml_tag = u"!base64"

    def __init__(self, value, encoding="utf-8"):
        self.value = value
        self.encoding = encoding

    @contextlib.asynccontextmanager
    async def open(self, emulator):
        payload = base64.b64decode(self.value.encode(self.encoding))
        transport = io.BytesIO(payload)
        async with Stream(transport) as stream:
            yield stream

class JinjaFileSystemLoader(jinja2.FileSystemLoader, YAMLObject):
    yaml_tag = u"!jinja/fsloader"


class JinjaTemplate(YAMLObject):

    yaml_tag = u"!template/jinja"

    def __init__(self, name, context, loader=None, encoding="utf-8"):
        self.name = name
        self.context = context
        self.loader = loader or JinjaFileSystemLoader(".")
        self.encoding = encoding

    @contextlib.asynccontextmanager
    async def open(self, emulator):

        env = jinja2.Environment(
            loader=self.loader,
        )
        template = env.get_template(self.name)
        value = template.render(self.context)

        transport = io.BytesIO(value.encode(self.encoding))

        async with Stream(transport) as stream:
            yield stream


class Execution:

    def __init__(self, emulator, script):
        self.emulator = emulator
        self.script = script

    # https://github.com/bminor/bash/blob/master/execute_cmd.c#L555
    async def run(self):
        logger.info("running execution")
        status = 0
        async with self.emulator.run() as executioner:
            logger.info("emulator started")
            for seq, statement in enumerate(self.script):
                logger.info("[%d] running %s",
                            seq, statement.yaml_tag)
                await statement.run(executioner)
        logger.debug("all done")

    def execute(self):
        asyncio.run(self.run())
