#!/usr/bin/env python3

"""aioconsole shim shammy stream

h'okay..i'd really not like to have to deal with the differences in
polling for files vs character devices vs sockets or whatever else
stdin might be. so, i'm leaning on aioconsole for providing the
NonFileStream* classes because they have a convenient
api. unfortunately for me, their streams try to do a lot of automatic
encoding and decoding if they notice bytes/strings, which isn't so
nice for me since my transport is all bytes and i don't want decoding!
thus one more layer of abstraction!  technically now this transport
carries messages wrapped in a shammy. this way aioconsole doesn't
notice the shammy is concealing bytes (shh) and doesn't try unicoding!
but then `sys.stdout.write` e.g. can't take a shammy :(. so, i need
this stream wrapper to unwrap the shammy..

"""


class Shammy:
    def __init__(self, data):
        # shh..
        self.data = data


class ShammyStream:

    def __init__(self, stream):
        self._stream = stream

    def write(self, shammy):
        return self._stream.write(shammy.data)

    def flush(self):
        if hasattr(self._stream, "flush"):
            return self._stream.flush()
        return None
