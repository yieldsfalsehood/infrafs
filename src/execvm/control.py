#!/usr/bin/env python3

import asyncio
import contextlib
import os
import stat
import subprocess
import sys

import aioconsole.stream

from . import jsh
from . import shim
from . import utils


class FileStreamReader:

    # implements asyncio.StreamReader for files

    def __init__(self, stream, loop):
        self.stream = stream
        self.loop = loop

    async def read(self, size):
        data = await self.loop.run_in_executor(None,
                                               self.stream.read,
                                               size)
        return data


class InputStream:

    def __init__(self, factory, stream, size=4096):
        self.factory = factory
        self.stream = stream
        self.size = size

    @contextlib.asynccontextmanager
    async def open(self, loop=None):
        reader = await self.factory(self.stream,
                                    loop or asyncio.get_event_loop())
        yield utils.AIterReader(reader, self.size)


async def open_file_stream_reader(stream, loop):
    return FileStreamReader(stream, loop)


async def open_non_file_stream_reader(stream, loop):

    reader = asyncio.StreamReader(loop=loop)
    protocol = asyncio.StreamReaderProtocol(reader)

    transport, _ = await loop.connect_read_pipe(
        lambda: protocol, stream
    )

    return reader


def istream(stream):

    mode = os.stat(stream.fileno()).st_mode

    factory = open_file_stream_reader if stat.S_ISREG(mode) else open_non_file_stream_reader

    return InputStream(factory, stream)


def ostream(stream):
    return aioconsole.stream.NonFileStreamWriter(
        shim.ShammyStream(stream)
    )


class Execution:

    def __init__(self, emulator, script,
                 stdin=None, stdout=None, stderr=None,
                 istreams=None, ostreams=None):

        self._emulator = emulator
        self._script = script

        self._stdin = stdin
        self._stdout = stdout
        self._stderr = stderr

        self._istreams = istreams
        self._ostreams = ostreams

    async def execute(self):

        istreams = {
            "stdin": self._stdin or istream(sys.stdin.buffer),
        } | (self._istreams or {})

        ostreams = {
            "stdout": self._stdout or ostream(sys.stdout.buffer),
            "stderr": self._stderr or ostream(sys.stderr.buffer),
        } | (self._ostreams or {})

        proc = await asyncio.create_subprocess_exec(
            *map(str, self._emulator.argv),
            stderr=sys.stderr,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )

        transport = jsh.Transport(proc.stdout, proc.stdin)
        server = jsh.Server(transport, self._script,
                            istreams, ostreams)

        status = await server.run()
        await proc.wait()

        return status

    # https://github.com/bminor/bash/blob/master/execute_cmd.c#L555
    async def execute(self):
        await self.emulator.run(self.script)

    def run(self):
        return asyncio.run(self.execute())
