#!/usr/bin/env python3


class AIter:

    def __init__(self, step):
        self._step = step

    async def __anext__(self):
        if data := await self._step():
            return data
        raise StopAsyncIteration


class AIterReader:

    def __init__(self, reader, size=4096):
        self.reader = reader
        self.size = size

    async def read(self):
        return await self.reader.read(self.size)

    def __aiter__(self):
        return AIter(self.read)
