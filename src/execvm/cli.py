#!/usr/bin/env python3

import argparse
import contextlib
import importlib
import logging
import sys

from . import control
from . import shell

logger = logging.getLogger("execvm")


def parse_args(argv):
    """Command line argument parser."""

    parser = argparse.ArgumentParser()

    scripts = parser.add_mutually_exclusive_group()
    scripts.add_argument("-c", help="load script from string")
    scripts.add_argument("-i", help="load script from file")

    emulators = parser.add_mutually_exclusive_group()
    emulators.add_argument("-m", help="load emulator from module")
    emulators.add_argument("--local", action="store_true",
                           default=False,
                           help="Use the local emulator")

    parser.add_argument("-e", "--env",
                        action="append")

    parser.add_argument("--ignore-environment", action="store_true",
                        default=False,
                        help="start with an empty environment")

    parser.add_argument("--verbose", "-v", action="count",
                        default=0,
                        help="Set output verbosity")

    subparsers = parser.add_subparsers(dest="emulator",
                                       help="emulator")

    parser_null = subparsers.add_parser(
        "null",
        help="null emulator",
    )

    parser_subprocess = subparsers.add_parser(
        "subprocess",
        help="subprocess emulator",
    )

    parser_jsh = subparsers.add_parser(
        "jsh",
        help="jsh emulator",
    )
    parser_jsh.add_argument("argv", nargs="+")

    return parser.parse_args(argv)


def configure_logging(args):

    if args.verbose > 1:
        logger.setLevel(logging.DEBUG)
    elif args.verbose > 0:
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.WARNING)

    handler = logging.StreamHandler()

    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    handler.setFormatter(formatter)

    logger.addHandler(handler)


def load(mspec):
    mname, fname = mspec.split(":", 1)
    module = importlib.import_module(mname)
    return getattr(module, fname)


def parse_emulator(args):

    if args.emulator == "null":
        return shell.NullEmulator()

    elif args.emulator == "subprocess":
        return shell.SubprocessEmulator()

    return shell.NullEmulator()


@contextlib.contextmanager
def open_script(args):

    if args.c:
        yield shell.Command.load_all(args.c)

    elif args.i:
        with open(args.i) as fin:
            yield shell.Command.load_all(fin)

    else:
        yield shell.Command.load_all(sys.stdin)


def main():

    args = parse_args(sys.argv[1:])

    configure_logging(args)

    emulator = parse_emulator(args)
    with open_script(args) as script:
        execution = shell.Execution(emulator, script)
        return execution.execute()
