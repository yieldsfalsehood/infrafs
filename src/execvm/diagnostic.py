#!/usr/bin/env python3

from . import shell


def cat():
    return shell.Script([
        shell.Command(
            argv=["cat", "-"],
            stdin="stdin",
        ),
    ])


def sh():
    return shell.Script([
        shell.Command(
            argv=["sh", "-s"],
            stdin="stdin",
        ),
    ])
