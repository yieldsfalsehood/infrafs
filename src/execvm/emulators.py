#!/usr/bin/env python3

from . import shell

import argparse
import itertools
import pathlib


def docker(_):
    return shell.Emulator(
        [
            "docker",
            "run",
            "-i",
            "--rm",
            "jsh",
        ]
    )


def cdrom(fname):
    return ["-cdrom", fname]


def drives(drive):
    return itertools.chain.from_iterable(
        ["-drive", fname] for fname in drive
    )


def initos(argv):

    parser = argparse.ArgumentParser()

    parser.add_argument("--qemu")
    parser.add_argument("--memory")
    parser.add_argument("--kernel-log")
    parser.add_argument("--initos")
    parser.add_argument("--cdrom")
    parser.add_argument("--drive", action="append")

    args = parser.parse_args(argv)

    initos = pathlib.Path(args.initos)

    return shell.Emulator([
        args.qemu,
        "-machine", "type=pc,accel=kvm",
        "-m", args.memory,
        "-nographic",
        "-no-reboot",
        "-monitor", "none",
        "-serial", f"{args.kernel_log}",
        "-device", "virtio-serial",
        "-chardev", "stdio,id=char0",
        "-device", "virtconsole,chardev=char0",
        "-nic", "user,model=e1000,mac=10:22:33:44:55:66",
        "-kernel", initos/"linux"/"vmlinuz",
        "-initrd", initos/"initos.cpio",
        "-append", "debug console=ttyS0 -- /dev/hvc0",
        *cdrom(args.cdrom),
        *drives(args.drive),
    ])
