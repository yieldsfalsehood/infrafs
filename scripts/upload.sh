#!/bin/bash

set -euo pipefail

hostname="$1"
fname="$2"
format="$3"

key="${hostname}.key"
bootvol="${hostname}-boot.qcow2"
rootvol="${hostname}-root.qcow2"

FNAME="$fname" FORMAT="$format" execvm -s infrafs.emulators:workstation \
     -i scripts/upload.yaml \
     -- \
     --qemu "qemu-system-x86_64" \
     --memory "1G" \
     --kernel-log "kernel.log" \
     --key "$key" \
     --boot "${bootvol}" \
     --root "${rootvol}"
