#!/bin/bash

set -euo pipefail

kernel="$1"
initrd="$2"

execvm -s infrafs.emulators:initos \
       -i scripts/msize.yaml \
       -- \
       --qemu "qemu-system-x86_64" \
       --memory "1G" \
       --kernel "$kernel" \
       --kernel-log "kernel.log" \
       --initrd "$initrd"
