#!/bin/bash

set -euo pipefail

hostname="$1"
uri="qemu+ssh://admin@host1/system"
pool="local-pool"

export INFRAFS_PROVISION_HOSTNAME="$hostname"
export INFRAFS_PROVISION_URI="$uri"
export INFRAFS_PROVISION_POOL="$pool"

workstation_hostname="workstation"
workstation_key="${workstation_hostname}.key"
workstation_bootvol="${workstation_hostname}-boot.qcow2"
workstation_rootvol="${workstation_hostname}-root.qcow2"

execvm -s infrafs.emulators:workstation \
       -m infrafs.scripts:provision \
       -- \
       --qemu "qemu-system-x86_64" \
       --memory "1G" \
       --kernel-log "kernel.log" \
       --key "${workstation_key}" \
       --boot "${workstation_bootvol}" \
       --root "${workstation_rootvol}"
