#!/bin/bash

set -euo pipefail

# i can run inspect the initrd locally like so without booting:
#
# cpio -id < ../initos/initos.initrd
# sudo env -i HOME=/ TERM=linux chroot . /init /dev/hvc0

qemu-system-x86_64 \
    -machine type=pc,accel=kvm \
    -m 128M \
    -nographic \
    -no-reboot \
    -monitor none \
    -serial file:hello.log \
    -device virtio-serial \
    -chardev stdio,id=char0 \
    -device virtconsole,chardev=char0 \
    -nic none \
    -initrd initos/initos.cpio \
    -kernel initos/linux/vmlinuz \
    -append "debug console=ttyS0 -- /dev/hvc0"
