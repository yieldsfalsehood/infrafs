#!/bin/bash

set -euo pipefail

kernel="$1"
initrd="$2"

# 1.97s virtioconsole / 4m 20.26s ttyS1
# head -c $((10*1024*1024)) /dev/zero

# 2m 51s virtioconsole
# head -c $((1024*1024*1024)) /dev/zero

head -c $((10*1024*1024)) /dev/zero \
    | execvm -s infrafs.emulators:initos \
             -c "--- !script
commands:
- !command
  argv:
  - time
  - wc
  - -c
  stdin: stdin
- !command
  argv:
  - poweroff
  - -f" \
             -- \
             --qemu "qemu-system-x86_64" \
             --memory "1G" \
             --kernel "$kernel" \
             --kernel-log "kernel.log" \
             --initrd "$initrd"
