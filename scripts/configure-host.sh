#!/bin/sh

set -euo pipefail

kernel="$1"
initrd="$2"
packages="$3"
alpine="$4"
ndqf="$5"

cluster=$(echo "$ndqf" | cut -d. -f1)
hostname=$(echo "$ndqf" | cut -d. -f2)

config="conf/${cluster}.yaml"

rootfs=$(yq -r ".hosts.${hostname}.volumes.root.uuid" < "$config")

key="${ndqf}.key"
bootvol="${ndqf}-boot.qcow2"
rootvol="${ndqf}-root.qcow2"

echo "unpacking files"

# file unpacking doesn't require a full boot or networking, so i'm
# just using the plain ol' initos qemu emulator. that's a little more
# orchestration for us, but it shaves off some unnecessary runtime.

cpace "conf/${cluster}.yaml" "$hostname" cpace.workstation \
    | ROOTFS="UUID=${rootfs}" execvm -s infrafs.emulators:installer \
            -i scripts/untar.yaml \
            -- \
            --qemu "qemu-system-x86_64" \
            --memory "256M" \
            --kernel "$kernel" \
            --kernel-log "${ndqf}-untar.log" \
            --initrd "$initrd" \
            --cdrom1 "$packages" \
            --cdrom2 "$alpine" \
            --key "$key" \
            --boot "$bootvol" \
            --root "$rootvol"

# for packages and services i don't need to run any commands that take
# stdin. i'd rather have a script generator pass the script on stdin,
# that way i can parameterize per host at this level. i don't actually
# do any on the fly production yet, i just have the flat yaml files
# for now :)

echo "running configure script"

execvm -s infrafs.emulators:runner \
       -- \
       --qemu "qemu-system-x86_64" \
       --memory "256M" \
       --kernel-log "${ndqf}-configure.log" \
       --key "$key" \
       --boot "$bootvol" \
       --root "$rootvol" \
       < "conf/${ndqf}/configure.yaml"
