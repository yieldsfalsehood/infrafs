#!/bin/bash

set -euo pipefail

BOOTFS=00000001-0000-0000-0001-000000000001
ROOTFS=00000001-0000-0000-0001-000000000002

qemu-img create -f raw boot.img 536870912
qemu-img create -f raw root.img 10737418240
mke2fs -F -t ext4 -U "${BOOTFS}" boot.img 524288
mke2fs -F -t ext4 -U "${ROOTFS}" root.img 10485760

# -m 128M
BOOTFS=UUID="${BOOTFS}" ROOTFS=UUID="${ROOTFS}" execvm -i scripts/install.yaml \
    qemu-system-x86_64 \
    -machine type=pc,accel=kvm \
    -m 8G \
    -nographic \
    -no-reboot \
    -monitor none \
    -serial file:install.log \
    -device virtio-serial \
    -chardev stdio,id=char0 \
    -device virtconsole,chardev=char0 \
    -nic user,model=e1000,mac=10:22:33:44:55:66 \
    -initrd initos/initos.cpio \
    -kernel initos/linux/vmlinuz \
    -cdrom alpine-minirootfs-3.14.3-x86_64.iso \
    -drive file=boot.img,format=raw \
    -drive file=root.img,format=raw \
    -append "debug console=ttyS0 -- /dev/hvc0"
