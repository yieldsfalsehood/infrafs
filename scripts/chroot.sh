#!/bin/bash

set -euo pipefail

BOOTFS=00000001-0000-0000-0001-000000000001
ROOTFS=00000001-0000-0000-0001-000000000002

execvm -i <(cat src/infrafs/scripts/network.yaml \
                <(infrafs-cmd \
                      mount UUID="${ROOTFS}" "/mnt") \
                <(infrafs-cmd \
                      mount UUID="${BOOTFS}" "/mnt/boot") \
                <(infrafs-cmd \
                      chroot --stdin "/mnt" "jsh")) \
       -- infrafs-emu initos \
       qemu-system-x86_64 \
       initos/initos.cpio \
       initos/linux/vmlinuz \
       128M \
       --kernel-log file:chroot.log \
       --drive file=boot.img,format=raw \
       --drive file=root.img,format=raw
