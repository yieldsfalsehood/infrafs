#!/bin/bash

set -euo pipefail

cat scripts/hello.yaml \
    src/infrafs/scripts/poweroff.yaml \
    | execvm -- infrafs-emu initos \
             qemu-system-x86_64 \
             initos/initos.cpio \
             initos/linux/vmlinuz \
             128M \
             --kernel-log file:hello2.log \
             --cdrom alpine-minirootfs-3.14.3-x86_64.iso
