#!/bin/bash

set -euo pipefail

cat initos/jsh/scripts/{hello,bye}.json \
    | infrafs-emu initos \
                  qemu-system-x86_64 \
                  initos/initos.cpio \
                  initos/linux/vmlinuz \
                  128M \
                  --kernel-log file:initos.log
