#!/bin/bash

set -euo pipefail

kernel="$1"
initrd="$2"
packages="$3"
alpine="$4"
ndqf="$5"

cluster=$(echo "$ndqf" | cut -d. -f1)
hostname=$(echo "$ndqf" | cut -d. -f2)

config="conf/${cluster}.yaml"

bootfs=$(yq -r ".hosts.${hostname}.volumes.boot.uuid" < "$config")
rootfs=$(yq -r ".hosts.${hostname}.volumes.root.uuid" < "$config")

# in bytes
bootsize=$(yq -r ".hosts.${hostname}.volumes.boot.size" < "$config")
rootsize=$(yq -r ".hosts.${hostname}.volumes.root.size" < "$config")

prepare_volume() {

    fname="$1"
    size="$2"
    uuid="$3"

    qemu-img create -f raw "$fname" "$size"
    mke2fs -F -t ext4 -U "$uuid" "$fname" "$((${size} / 1024))"

}

encrypt_volume() {

    ifname="$1"
    ofname="$2"
    key="$3"

    qemu-img convert \
        -f raw \
        -O qcow2 \
        --object secret,id=sec0,file="$key" \
        -o encrypt.format=luks \
        -o encrypt.key-secret=sec0 \
        "$ifname" "$ofname"

}

key="${ndqf}.key"
bootvol="${ndqf}-boot"
rootvol="${ndqf}-root"

head -c 32 /dev/zero | base64 - > "${key}"

prepare_volume "${bootvol}.img" "$bootsize" "$bootfs"
encrypt_volume "${bootvol}.img" "${bootvol}.qcow2" "$key"

prepare_volume "${rootvol}.img" "$rootsize" "$rootfs"
encrypt_volume "${rootvol}.img" "${rootvol}.qcow2" "$key"

cpace "conf/${cluster}.yaml" "$hostname" cpace.workstation \
    | BOOTFS="UUID=${bootfs}" ROOTFS="UUID=${rootfs}" \
            execvm -s infrafs.emulators:installer \
            -i scripts/install.yaml \
            -- \
            --qemu "qemu-system-x86_64" \
            --memory "256M" \
            --kernel "$kernel" \
            --kernel-log "${ndqf}-install.log" \
            --initrd "$initrd" \
            --cdrom1 "$packages" \
            --cdrom2 "$alpine" \
            --key "$key" \
            --boot "${bootvol}.qcow2" \
            --root "${rootvol}.qcow2"
