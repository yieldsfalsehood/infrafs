#!/bin/bash

set -euo pipefail

execvm -i scripts/os-release.yaml \
       -- bash scripts/chroot.sh

# infrafs chroot workstation
