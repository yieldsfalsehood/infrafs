#!/bin/bash

set -euo pipefail

BOOTFS=00000001-0000-0000-0001-000000000001
ROOTFS=00000001-0000-0000-0001-000000000002

cpace conf/cluster1.yaml workstation cpace.workstation \
    | execvm -i scripts/untar.yaml \
             -- infrafs-emu chroot \
             qemu-system-x86_64 \
             initos/initos.cpio \
             initos/linux/vmlinuz \
             128M \
             "${BOOTFS}" \
             "${ROOTFS}" \
             --kernel-log file:prepare.log \
             --drive file=boot.img,format=raw \
             --drive file=root.img,format=raw
