#!/bin/bash

set -euo pipefail

qemu-system-x86_64 \
    -machine type=pc,accel=kvm \
    -m 128M \
    -nographic \
    -no-reboot \
    -monitor none \
    -serial stdio \
    -nic none \
    -initrd "${INITOS_INITRD}" \
    -kernel "${INITOS_KERNEL}" \
    -append "debug console=ttyS0 rdinit=/bin/sh"
