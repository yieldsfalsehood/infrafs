#!/bin/bash

set -euo pipefail

hostname="$1"

key="${hostname}.key"
bootvol="${hostname}-boot.qcow2"
rootvol="${hostname}-root.qcow2"

# ctrl-a c to enter/exit the monitor

qemu-system-x86_64 \
  -machine type=pc,accel=kvm \
  --object secret,id=sec0,file="$key" \
  -drive file="$bootvol",format=qcow2,if=virtio,encrypt.key-secret=sec0 \
  -drive file="$rootvol",format=qcow2,if=virtio,encrypt.key-secret=sec0 \
  -m 256M \
  -nic user,mac=52:54:00:12:34:55 \
  -nic vde,sock=vlan1.switch,mac=52:54:00:12:34:56 \
  -nographic \
  -device virtio-serial \
  -chardev null,id=char0 \
  -device virtconsole,chardev=char0 \
  -serial file:"${hostname}.log" \
  -serial mon:stdio \
  -pidfile "${hostname}.pid"
