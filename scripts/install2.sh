#!/bin/bash

set -euo pipefail

BOOTFS=00000001-0000-0000-0001-000000000001
ROOTFS=00000001-0000-0000-0001-000000000002

qemu-img create -f raw boot.img 536870912
qemu-img create -f raw root.img 10737418240
mke2fs -F -t ext4 -U "${BOOTFS}" boot.img 524288
mke2fs -F -t ext4 -U "${ROOTFS}" root.img 10485760

cat src/infrafs/scripts/install/setup.yaml \
    <(infrafs-cmd mount UUID="${ROOTFS}" "/opt/mnt") \
    src/infrafs/scripts/install/boot.yaml \
    <(infrafs-cmd mount UUID="${BOOTFS}" "/opt/mnt/boot") \
    src/infrafs/scripts/install/alpine.yaml \
    src/infrafs/scripts/poweroff.yaml \
    | execvm -- infrafs-emu initos \
             qemu-system-x86_64 \
             initos/initos.cpio \
             initos/linux/vmlinuz \
             8G \
             --kernel-log file:install.log \
             --cdrom alpine-minirootfs-3.14.3-x86_64.iso \
             --drive file=boot.img,format=raw \
             --drive file=root.img,format=raw

# infrafs-cmd install \
#             alpine \
#             00000001-0000-0000-0001-000000000001 \
#             00000001-0000-0000-0001-000000000002
