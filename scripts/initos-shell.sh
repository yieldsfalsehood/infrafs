#!/bin/

set -euo pipefail

kernel="$1"
initrd="$2"

qemu-system-x86_64 \
    -machine type=pc,accel=kvm \
    -m 1G \
    -nographic \
    -no-reboot \
    -monitor none \
    -serial file:kernel.log \
    -device virtio-serial \
    -chardev stdio,id=char0 \
    -device virtconsole,chardev=char0 \
    -smp $(nproc) \
    -nic none \
    -kernel "$kernel" \
    -initrd "$initrd" \
    -append "debug console=ttyS0 -- /dev/hvc0"
