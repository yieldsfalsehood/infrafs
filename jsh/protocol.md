
shell2c
* helo
* receipt (jid, iid, size)
* output (jid, stream[{stderr,stdout}], sid, payload)
* stopped (jid, status)

c2shell
* start (jid, argv, envp, dir)
* input (jid, iid, payload)
* eof (jid)
