package main

import (
	"os"
)

import (
	"gitlab.com/yieldsfalsehood/infrafs/initos/jsh/internal/jsh"
)

func main() {
	jsh.Main(os.Args)
}
