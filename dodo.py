#!/usr/bin/env python3

import os

from doit.action import CmdAction


BOOTFS = "00000001-0000-0000-0001-000000000001"
ROOTFS = "00000001-0000-0000-0001-000000000002"


def task_prepare_volumes():
    return {
        "actions": [
            ["qemu-img", "create",
             "-f", "raw",
             "boot.img", "536870912",
            ],
            ["qemu-img", "create",
             "-f", "raw",
             "root.img", "10737418240",
            ],
            ["mke2fs", "-F", "-t", "ext4",
             "-U", BOOTFS,
             "boot.img", "524288",
            ],
            ["mke2fs", "-F", "-t", "ext4",
             "-U", ROOTFS,
             "root.img", "10485760",
            ]
        ]
    }


def task_install():

    env = {
        "BOOTFS": f"UUID={BOOTFS}",
        "ROOTFS": f"UUID={ROOTFS}",
    }

    return {
        "actions": [
            CmdAction(
                ["execvm",
                 "-i", "scripts/install.yaml",
                 "-s", "execvm.emulators:initos",
                 "--",
                 "--qemu", "qemu-system-x86_64",
                 "--memory", "256M",
                 "--kernel-log", "file:install.log",
                 "--initos", "initos",
                 "--cdrom", "alpine-minirootfs-3.14.3-x86_64.iso",
                 "--drive", "file=boot.img,format=raw",
                 "--drive", "file=root.img,format=raw",
                 ],
                env={**os.environ, **env},
                shell=False,
            )
        ]
    }
