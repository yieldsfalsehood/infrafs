
# infrafs

home lab infrastructure automation, or: how I spent my winter vacation

# introduction

This project is comprised of four tools and one bespoke OS. To build
and use it you will need:

* go
* python3
* make
* bsdtar
* an internet connection

## Building and Installing

This project provides both a go package and a python package, both of
which install cli components. For my environment I use a virtualenv
and set `GOBIN="${VIRTUAL_ENV:-}"/bin` when installing and all
commands are tested as written.

### Installation

#### From git

```
$ go install gitlab.com/yieldsfalsehood/infrafs.git@dev
$ python3 -m pip install git+https://gitlab.com/yieldsfalsehood/infrafs.git@dev
```

#### From source

```
$ make
$ make install
```

## background

For background on this project see [this
post](https://gitlab.com/yields.falsehood/infrafs/-/blob/demo/post.md).

check kubevirt and https://firecracker-microvm.github.io/!
found this blog https://mergeboard.com/blog/2-qemu-microvm-docker/
and this blog https://blog.stefan-koch.name/2020/05/31/automation-archlinux-qemu-installation

# tools and artifacts

## jsh

## execvm

```
$ execvm -c '!command {argv: [echo, hello, world]}' subprocess
```

```
$ execvm subprocess < script.yaml
$ execvm -i script.yaml subprocess
```

## initos

### Specification

`initos` is built by layering the filesystem hierarchy, busybox,
initos-specific operating system components, the jsh package, and
instance-specific configuration.

```
FHS
busybox
  /bin/busybox
  <symlinks>
initos
  /etc/os-release
  /init
  /usr/bin/autologin
  /usr/bin/jetty
  /usr/bin/jogin
jsh
  /usr/bin/jsh
config
  /etc/network/interfaces
  /etc/mactab
    lan0 12:34:56:78:9a:de
  /etc/mdev.conf
    -net/.*         root:root 600 @/sbin/nameif -s
```

This way, `initos` is constructed similarly to the guests
themselves. The difference is only in process since we don't have
anything like an installer with a priori mount points. In fact, there
are no character or block devices specified by or reserved for initos
so there isn't even a "thing to mount". There is neither an `fstab`
nor a bootloader.

The initos kernel has support for network devices, so users may
provide network and DNS configuration same as for other guests if
those are needed, but the OS does not specify or require any
particular devices or nameservers. `infrafs` will populate
configuration in the image, but with no init _daemon_ you will also
need to call `mdev` and `ifup` when needed.

The `init` process that does run will prepare standard virtual mounts,
run `jsh` attached to a terminal, then shutdown. The `jsh` process
will be run with a writable root and this `PATH`:

```
/usr/local/sbin
/usr/local/bin
/usr/sbin
/usr/bin
/sbin
/bin
```

## cpace

### Configuration Model

```
networks = {
  "host": HostNetwork(),
  "vlan1": VdeNetwork(
    domain="domain.name",
    address="0.0.0.0",
    netmask="255.255.255.255",
    gateway="0.0.0.1",
  )
}
```

```
hosts = {
  "host": Host(
    arch="x86_64",
    volumes={
      "boot": Volume(uuid="xx1", size="yyy"),
      "root": Volume(uuid="xx2", size="yyy"),
      "srv": Volume(uuid="xx3", size="yyy"),
    },
    interfaces={
      "vlan1": NetworkInterface(
        address="ba:be:01:00:01:01",
        network=networks["vlan1"],
        family="inet",
        method="dhcp",
      )
    }
  ),
}
```

### Configuration

https://forum.mangoh.io/t/using-mdev-to-assign-names-to-usb-network-interfaces/2237
https://gist.github.com/jirutka/9496300

## infrafs

### pre-requisites

* e2fsprogs
* execvm
* make
* python3
* qemu
* util-linux

```
$ infrafs -c conf/cluster1.yaml prepare-initos-initrd
$ infrafs -c conf/cluster1.yaml prepare-initos-kernel
$ infrafs -c conf/cluster1.yaml prepare-installer
$ infrafs -c conf/cluster1.yaml provision
$ infrafs -c conf/cluster1.yaml start-network
$ infrafs -c conf/cluster1.yaml start
$ infrafs -c conf/cluster1.yaml xvncviewer workstation
```
